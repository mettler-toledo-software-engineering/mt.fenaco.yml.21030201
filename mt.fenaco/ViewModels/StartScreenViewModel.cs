﻿using log4net;
using mt.fenaco.Infrastructure;
using mt.fenaco.Logic;
using mt.fenaco.Views;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace mt.fenaco.ViewModels
{
    public class StartScreenViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(StartScreenViewModel);

        private Settings _currentSettings;

        private DelegateCommand _goShowOverviewCommand;
        private DelegateCommand _goZeroCommand;
        private DelegateCommand _goTareCommand;
        private DelegateCommand _goClearTareCommand;
        
        public StartScreenViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, ISecurityService securityService)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Settings.Current.Save();
            _currentSettings = Settings.Current;

            InitializeStaticVariables();
            GoClearTareCommandExecute();
        }

        #region Initialization       
        
        private void InitializeStaticVariables()
        {
            Version = Globals.GetVersion();
            ProjectNumber = Globals.ProjectNumber;
        }

        #endregion

        #region KeyHandling

        #region ---- GoShowOverview

        public ICommand GoShowOverview => _goShowOverviewCommand = new DelegateCommand(GoShowOverviewCommandExecute);
        private InfoBoxView _infoBox;

        private async void GoShowOverviewCommandExecute()
        {
            string title = Localization.Get(Localization.Key.LoadRecipeTitle);
            string message = Localization.Get(Localization.Key.LoadRecipeMessage);

            _infoBox = new InfoBoxView(title, message, MessageBoxIcon.Information, MessageBoxButtons.OK, false);
            _infoBox.Show(_parent);

            await DatabaseExtension.GetAllOrdersFromDBAsync();

            _infoBox.Close();
            _homeNavigationFrame.NavigateTo(new OrderSelectionView(_homeNavigationFrame), TransitionAnimation.LeftToRight);
        }

        #endregion

        #region ---- Zero
        
        private void GoZeroCommandExecute()
        {
            Globals.DoZero();
        }

        public ICommand GoZero => _goZeroCommand = new DelegateCommand(GoZeroCommandExecute);
        #endregion

        #region ---- Tare
        
        private void GoTareCommandExecute()
        {
            Globals.DoTare();
        }

        public ICommand GoTare => _goTareCommand = new DelegateCommand(GoTareCommandExecute);
        #endregion

        #region ---- ClearTare

        private void GoClearTareCommandExecute()
        {
            Globals.DoClearTare();
        }

        public ICommand GoClearTare => _goClearTareCommand = new DelegateCommand(GoClearTareCommandExecute);


        #endregion

        #endregion

        #region Properties

        #region ---- ProjectNumber
        private string _projectNumber = "";
        public string ProjectNumber
        {
            get
            {
                return _projectNumber;
            }
            set
            {
                if (_projectNumber != value)
                {
                    _projectNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- Version
        private string _version = "";
        public string Version
        {
            get
            {
                return _version;
            }
            set
            {
                if (_version != value)
                {
                    _version = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #endregion
    }
}