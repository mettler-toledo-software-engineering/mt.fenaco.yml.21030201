﻿using mt.fenaco.Views;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using System;

namespace mt.fenaco.ViewModels
{
    public class AmountInputViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedContentControl _homeNavigationFrame;

        private DelegateCommand _cancelCommand = null;
        private DelegateCommand _saveCommand = null;

        public AmountInputViewModel(Visual parent, AnimatedContentControl homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Amount = 1;
        }

        #region Amount
        private int _amount;

        public int Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                if (value != _amount)
                {
                    _amount = value;
                    NotifyPropertyChanged();                    
                    _saveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region GoCancel
        public ICommand GoCancel => _cancelCommand = new DelegateCommand(CancelCommandExecute);

        private void CancelCommandExecute()
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            ((AmountInputView)_parent).Close();
        }
        #endregion

        #region GoSave
        public ICommand GoSave => _saveCommand = new DelegateCommand(SaveCommandExecute, CanExecuteAddSaveCommand);

        private void SaveCommandExecute()
        {
            OnAmountSaved(new AmountSavedEventArgs(Amount));
            CloseWindow();
        }

        private bool CanExecuteAddSaveCommand()
        {
            return Amount > 0;
        }

        #endregion

        #region Event OnAmountSaved

        public event EventHandler<AmountSavedEventArgs> AmountSaved;

        protected virtual void OnAmountSaved(AmountSavedEventArgs e)
        {
            if (AmountSaved != null)
                AmountSaved(this, e);
        }

        public class AmountSavedEventArgs : EventArgs
        {
            public int Amount { get; set; }

            public AmountSavedEventArgs(int amount)
            {
                Amount = amount;
            }
        }

        #endregion
    }
}
