﻿using log4net;
using mt.fenaco.Infrastructure;
using mt.fenaco.Logic;
using mt.fenaco.Models;
using mt.fenaco.Views;
using mt.fenaco.Views.Controls;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mt.fenaco.ViewModels
{
    public class FillOverviewViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(FillProcessViewModel);

        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private DelegateCommand _goCancelCommand;
        private DelegateCommand _goEditWeighingCommand;
        private DelegateCommand _goDeleteWeighingCommand;
        private DelegateCommand _goFinishCommand;

        private List<WeighingModel> _flowerWeighings;
        private List<WeighingModel> _grassWeighings;

        public FillOverviewViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, List<WeighingModel> flowerWeighings, List<WeighingModel> grassWeighings)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _flowerWeighings = flowerWeighings;
            _grassWeighings = grassWeighings;

            LoadRecipeControls();
        }

        #region Initialize

        private void LoadRecipeControls()
        {
            FlowerComponents = new RecipeControlView(_homeNavigationFrame, Localization.Get(Localization.Key.Flower), _flowerWeighings, Globals.WeighingRecipeWidth);
            FlowerComponents._viewModel.WeighingItemSelected += _viewModel_WeighingItemSelected;

            GrassComponents = new RecipeControlView(_homeNavigationFrame, Localization.Get(Localization.Key.Grass), _grassWeighings, Globals.WeighingRecipeWidth);
            GrassComponents._viewModel.WeighingItemSelected += _viewModel_WeighingItemSelected;
        }

        private void _viewModel_WeighingItemSelected(object sender, Controls.RecipeControlViewModel.WeighingItemSelectedEventArgs e)
        {
            SelectedWeighingItemDetail = e.Weighing;
            NotifyCanExecuteChanged();
        }

        private void NotifyCanExecuteChanged()
        {
            _goEditWeighingCommand?.NotifyCanExecuteChanged();
            _goDeleteWeighingCommand?.NotifyCanExecuteChanged();
            _goFinishCommand?.NotifyCanExecuteChanged();
        }

        #endregion

        #region Properties

        #region FlowerComponents

        private RecipeControlView _flowerComponents;

        public RecipeControlView FlowerComponents
        {
            get
            {
                return _flowerComponents;
            }
            set
            {
                if (value != _flowerComponents)
                {
                    _flowerComponents = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region GrassComponents

        private RecipeControlView _grassComponents;

        public RecipeControlView GrassComponents
        {
            get
            {
                return _grassComponents;
            }
            set
            {
                if (value != _grassComponents)
                {
                    _grassComponents = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion
        
        #region SelectedWeighingItemDetail

        private WeighingModel _selectedWeighingItemDetail;
        public WeighingModel SelectedWeighingItemDetail
        {
            get
            {
                return _selectedWeighingItemDetail;
            }
            set
            {
                if (value != _selectedWeighingItemDetail)
                {
                    _selectedWeighingItemDetail = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

        #region Key Commands

        #region GoCancel
        public ICommand GoCancel => _goCancelCommand = new DelegateCommand(GoBackCommandExecute);

        private void GoBackCommandExecute()
        {
            string title = Localization.Get(Localization.Key.CancelWeighing);
            string message = Localization.Get(Localization.Key.CancelWeighingMessage);

            InfoBoxView cancelQuestion = new InfoBoxView(title, message, MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            cancelQuestion.Closed += CancelQuestion_Closed;
            cancelQuestion.Show(_parent);
        }

        private void CancelQuestion_Closed(object sender, EventArgs e)
        {
            if (((InfoBoxView)sender).ViewModel.Result == DialogResult.Yes)
            {
                _log.InfoEx($"cancel", _sourceClass);

                _homeNavigationFrame.Back();
                OnCancelled(new CancelledEventArgs());
            }
        }

        #endregion

        #region GoEditWeighing

        public ICommand GoEditWeighing => _goEditWeighingCommand = new DelegateCommand(GoEditWeighingCommandExecute, CanExecuteGoEditWeighingCommand);

        private void GoEditWeighingCommandExecute()
        {
            //UnregisterEventsForViewModel();
            
            AddManualInfosView editWindow = new AddManualInfosView(_homeNavigationFrame, SelectedWeighingItemDetail, SelectedWeighingItemDetail.Weight, false);
            editWindow._viewModel.Saved += _viewModel_WeighingItemUpdated;
            editWindow._viewModel.Closed += _viewModel_Closed;
            editWindow.Show(_parent);
        }

        private void _viewModel_Closed(object sender, AddManualInfosViewModel.ClosedEventArgs e)
        {
            ResetSelectedWeighingItemDetail();
            NotifyCanExecuteChanged();

            //RegisterBarcodeReader();
            //RegisterEventsForViewModel();
        }

        private void _viewModel_WeighingItemUpdated(object sender, AddManualInfosViewModel.SavedEventArgs e)
        {
            SelectedWeighingItemDetail.Charge = e.Charge;
            SelectedWeighingItemDetail.ProductNr = e.ProductNr;
            SelectedWeighingItemDetail.Weight = e.Weight;

            UpdateWeighingItemInDBAsync().ContinueWith(CurrentInfosSavedInDB);
        }

        private void CurrentInfosSavedInDB(Task task)
        {
            if (task.Exception == null)
            {
                _log.InfoEx($"current infos saved", _sourceClass);

                LoadRecipeControls();
            }
            else
            {
                _log.ErrorEx($"save current infos failed", _sourceClass);
            }
        }

        private async Task UpdateWeighingItemInDBAsync()
        {
            _log.InfoEx($"update weighingitem {SelectedWeighingItemDetail.ToString()}", _sourceClass);

            await DatabaseExtension.UpdateWeighingInDBAsync(SelectedWeighingItemDetail);
        }

        private bool CanExecuteGoEditWeighingCommand()
        {
            return SelectedWeighingItemDetail != null;
        }

        private void ResetSelectedWeighingItemDetail()
        {
            SelectedWeighingItemDetail = null;
        }

        #endregion

        #region GoDeleteWeighing

        public ICommand GoDeleteWeighing => _goDeleteWeighingCommand = new DelegateCommand(GoDeleteWeighingCommandExecute, CanDeleteGoEditWeighingCommand);

        private void GoDeleteWeighingCommandExecute()
        {
            string title = Localization.Get(Localization.Key.DeleteWeighing);
            string message = Localization.Get(Localization.Key.DeleteWeighingMessage);

            InfoBoxView infoDeleteWeighing = new InfoBoxView(title, message, MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            infoDeleteWeighing.Closed += InfoDeleteWeighing_Closed;
            infoDeleteWeighing.Show(_parent);
        }

        private async void InfoDeleteWeighing_Closed(object sender, EventArgs e)
        {
            if (((InfoBoxView)sender).ViewModel.Result == DialogResult.Yes)
            {
                WeighingModel parent = DatabaseExtension.Weighings.Where(item => item.Id == SelectedWeighingItemDetail.ParentId).FirstOrDefault();

                if (parent != null)
                {
                    parent.IsDone = false;
                    await DatabaseExtension.DeleteWeighingAsync(SelectedWeighingItemDetail);
                }
                else
                {
                    parent = SelectedWeighingItemDetail;
                    parent.IsDone = false;
                    parent.Charge = null;
                    parent.ProductNr = null;
                    parent.Weight = 0;
                }

                await DatabaseExtension.UpdateWeighingInDBAsync(parent);

                ResetSelectedWeighingItemDetail();
                LoadRecipeControls();
                NotifyCanExecuteChanged();
            }
        }

        private bool CanDeleteGoEditWeighingCommand()
        {
            return SelectedWeighingItemDetail != null;
        }

        #endregion

        #region GoFinish

        public ICommand GoFinish => _goFinishCommand = new DelegateCommand(GoFinishCommandExecute, CanExecuteFinishCommand);

        private void GoFinishCommandExecute()
        {
            string title = Localization.Get(Localization.Key.CompleteRecipeTitle);
            string message = Localization.Get(Localization.Key.CompleteRecipeQuestion);

            InfoBoxView completeQuestion = new InfoBoxView(title, message, MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            completeQuestion.Closed += CompleteQuestion_Closed;
            completeQuestion.Show(_parent);
        }

        private void CompleteQuestion_Closed(object sender, EventArgs e)
        {
            if (((InfoBoxView)sender).ViewModel.Result == DialogResult.Yes)
            {
                _log.InfoEx($"complete recipe", _sourceClass);

                _homeNavigationFrame.Back();
                OnRecipeCompleted(new RecipeCompletedEventArgs());
            }
        }

        private bool CanExecuteFinishCommand()
        {
            return !HasOpenWeighings(_flowerWeighings) && !HasOpenWeighings(_grassWeighings);
        }

        private bool HasOpenWeighings(List<WeighingModel> weighingItems)
        {
            return weighingItems.Where(i => !i.IsDone).Count() > 0;
        }

        #endregion

        #endregion

        #region Event OnCancelled

        public event EventHandler<CancelledEventArgs> Cancelled;

        protected virtual void OnCancelled(CancelledEventArgs e)
        {
            if (Cancelled != null)
                Cancelled(this, e);
        }

        public class CancelledEventArgs : EventArgs
        {

            public CancelledEventArgs()
            {
            }
        }

        #endregion

        #region Event OnRecipeCompleted

        public event EventHandler<RecipeCompletedEventArgs> RecipeCompleted;

        protected virtual void OnRecipeCompleted(RecipeCompletedEventArgs e)
        {
            if (RecipeCompleted != null)
                RecipeCompleted(this, e);
        }

        public class RecipeCompletedEventArgs : EventArgs
        {

            public RecipeCompletedEventArgs()
            {
            }
        }

        #endregion
    }
}
