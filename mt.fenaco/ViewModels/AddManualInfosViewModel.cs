﻿using log4net;
using mt.fenaco.Infrastructure;
using mt.fenaco.Logic.BarcodeReader;
using mt.fenaco.Models;
using mt.fenaco.Views;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using System;
using System.Threading;

namespace mt.fenaco.ViewModels
{
    public class AddManualInfosViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(AddManualInfosViewModel);

        private readonly Visual _parent;
        private readonly AnimatedContentControl _homeNavigationFrame;
        
        private DelegateCommand _cancelCommand = null;
        private DelegateCommand _saveCommand = null;
        private IBarcodeReader _barcodeReader;

        private WeighingModel _weighingItem;
        private bool _isNewWeighing;

        public AddManualInfosViewModel(Visual parent, AnimatedContentControl homeNavigationFrame, WeighingModel weighingItem, double weight, bool isNew = true)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _weighingItem = weighingItem;
            _isNewWeighing = isNew;

            Initialize(weight);

            RegisterBarcodeReader();
            RegisterEventsForViewModel();
        }

        #region Initialize

        private void Initialize(double weight)
        {
            if (!_isNewWeighing)
            {
                LoadFields();
            }
            else
            {
                Weight = Globals.RoundValue(weight);
            }

            Unit = Globals.GetCurrentScaleUnit();
        }

        private void LoadFields()
        {
            Charge = _weighingItem.Charge;
            ArticleNr = _weighingItem.ProductNr;
            Weight = _weighingItem.Weight;
        }

        #endregion

        #region Charge
        private string _charge;

        public string Charge
        {
            get
            {
                return _charge;
            }
            set
            {
                if (value != _charge)
                {
                    _charge = value;
                    NotifyPropertyChanged();
                    _saveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region ArticleNr
        private string _articleNr;

        public string ArticleNr
        {
            get
            {
                return _articleNr;
            }
            set
            {
                if (value != _articleNr)
                {
                    _articleNr = value;
                    NotifyPropertyChanged();
                    _saveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region Weight
        private double _weight;

        public double Weight
        {
            get
            {
                return _weight;
            }
            set
            {
                if (value != _weight)
                {
                    _weight = value;
                    NotifyPropertyChanged();
                    _saveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region Unit
        private string _unit;

        public string Unit
        {
            get
            {
                return _unit;
            }
            set
            {
                if (value != _unit)
                {
                    _unit = value;
                    NotifyPropertyChanged();
                    _saveCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        #endregion

        #region GoCancel
        public ICommand GoCancel => _cancelCommand = new DelegateCommand(CancelCommandExecute);

        private void CancelCommandExecute()
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            UnregisterEventsForViewModel();
            OnClosed(new ClosedEventArgs());
            ((AddManualInfosView)_parent).Close();
        }
        #endregion

        #region GoSave
        public ICommand GoSave => _saveCommand = new DelegateCommand(SaveCommandExecute);

        private void SaveCommandExecute()
        {
            OnSaved(new SavedEventArgs(Charge, ArticleNr, Weight, _isNewWeighing));

            CloseWindow();
        }

        #endregion

        #region Event OnSaved

        public event EventHandler<SavedEventArgs> Saved;

        protected virtual void OnSaved(SavedEventArgs e)
        {
            if (Saved != null)
                Saved(this, e);
        }

        public class SavedEventArgs : EventArgs
        {
            public string ProductNr { get; set; }
            public string Charge { get; set; }
            public double Weight { get; set; }
            public bool IsNewWeighing { get; set; }

            public SavedEventArgs(string charge, string lot, double weight, bool isNew)
            {
                ProductNr = lot;
                Charge = charge;
                Weight = weight;
                IsNewWeighing = isNew;
            }
        }

        #endregion

        #region Event OnClosed

        public event EventHandler<ClosedEventArgs> Closed;

        protected virtual void OnClosed(ClosedEventArgs e)
        {
            if (Closed != null)
                Closed(this, e);
        }

        public class ClosedEventArgs : EventArgs
        {
            public ClosedEventArgs()
            {
            }
        }

        #endregion

        #region RegisterEvents

        private void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }

        private void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                _log.InfoEx($"barcode reader on viewmodel ready", _sourceClass);
                return;
            }

            _log.Error($"barcode reader on viewmodel NULL");
        }

        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            _log.InfoEx($"barcode reader on viewmodel data received {e.Data}", _sourceClass);

            if (e.ArticleName == _weighingItem.RecipeItem.NameBot)
            {
                Charge = e.Charge;
                ArticleNr = e.ArticleNumber;
            }
            else
            {
                string title = Localization.Get(Localization.Key.Scannfailed);
                string message = Localization.Get(Localization.Key.WrongItemScanned);

                InfoBoxView info = new InfoBoxView(title, message, MessageBoxIcon.Error, MessageBoxButtons.OK, false);
                info.Show(_parent);
                Thread.Sleep(3500);
                info.Close();

                _log.ErrorEx($"wrong item scanned!", _sourceClass);
            }
        }

        #endregion

        #region UnregisterEvents
        private void UnregisterEventsForViewModel()
        {
            _log.InfoEx($"barcode reader on viewmodel NULL", _sourceClass);

            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
            _barcodeReader = null;
        }

        #endregion
    }
}
