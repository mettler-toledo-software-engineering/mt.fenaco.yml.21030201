﻿using mt.fenaco.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Drawing;
using System;

namespace mt.fenaco.ViewModels.Controls
{
    public class WeighingItemDetailControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        public WeighingItemDetailControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, WeighingModel weighing)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            WeighingItem = weighing;
        }

        public void SelectItem()
        {
            OnWeighingItemClicked(new WeighingItemClickedEventArgs(WeighingItem));
        }

        #region Properties

        #region WeighingItem

        private WeighingModel _weighingItem;

        public WeighingModel WeighingItem
        {
            get { return _weighingItem; }
            set
            {
                if (value != _weighingItem)
                {
                    _weighingItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Background
        private SolidColorBrush _background;

        public SolidColorBrush Background
        {
            get { return _background; }
            set
            {
                if (value != _background)
                {
                    _background = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #endregion

        #region Event WeighingItemClickedEventArgs

        public event EventHandler<WeighingItemClickedEventArgs> WeighingItemClicked;

        protected virtual void OnWeighingItemClicked(WeighingItemClickedEventArgs e)
        {
            if (WeighingItemClicked != null)
                WeighingItemClicked(this, e);
        }

        public class WeighingItemClickedEventArgs : EventArgs
        {
            public WeighingModel Weighing { get; set; }

            public WeighingItemClickedEventArgs(WeighingModel weighing)
            {
                Weighing = weighing;
            }
        }

        #endregion
    }
}
