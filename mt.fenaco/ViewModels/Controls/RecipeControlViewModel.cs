﻿using mt.fenaco.Infrastructure;
using mt.fenaco.Models;
using mt.fenaco.Views.Controls;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Drawing;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace mt.fenaco.ViewModels.Controls
{
    public class RecipeControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private List<WeighingModel> _weighingItems = new List<WeighingModel>();
        private List<RecipeItemModel> _recipeItems = new List<RecipeItemModel>(Globals.MaxRecipeItems);
        private List<List<object>> _recipeItemListPages = new List<List<object>>();

        private DelegateCommand _pageBackwardsCommand = null;
        private DelegateCommand _pageForwardsCommand = null;

        private WeighingModel _firstWeighingItemInList;

        public RecipeControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, string title, List<RecipeItemModel> recipeItems, int listWidth)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _recipeItems = recipeItems;

            ListBoxWidth = listWidth;
            Title = title;
            
            LoadRecipeItems(Globals.MaxRecipeItems);
        }

        public RecipeControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, string title, List<WeighingModel> weighingItems, int listWidth)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _weighingItems = weighingItems;

            ListBoxWidth = listWidth;
            Title = title;

            LoadWeighingItems(Globals.MaxWeighingItems);
        }

        public void UpdateNextPage(int itemIndex, int pageIndex)
        {
            CurrentPageIndex = pageIndex;

            if (_recipeItemListPages[CurrentPageIndex][itemIndex] == _firstWeighingItemInList && CanExecutePageForwardsCommand())
            {
                PageForwardsCommandExecute();
            }
        }

        #region Item Overview

        public void LoadWeighingItems(int maxItemsPerPage)
        {
            int lastWeighingItemIndex = _weighingItems.Count - 1;
            
            if (lastWeighingItemIndex >= 0)
            {
                WeighingModel lastModel = _weighingItems[lastWeighingItemIndex];
                List<object> recipeItemList = new List<object>(maxItemsPerPage);
                
                foreach (WeighingModel item in _weighingItems)
                {
                    recipeItemList.Add(item);                    

                    if (recipeItemList.Count == maxItemsPerPage || item == lastModel)
                    {
                        _recipeItemListPages.Add(recipeItemList);
                        recipeItemList = new List<object>(maxItemsPerPage);
                    }
                }

                LastPageIndex = _recipeItemListPages.Count - 1;

                LoadCurrentRecipeItemDetailPage();
            }
        }

        public void LoadCurrentRecipeItemDetailPage()
        {
            ItemList = new List<object>();

            List<object> currentRecipeItems = _recipeItemListPages[CurrentPageIndex];

            SetFirstWeighingItemInList();

            foreach (WeighingModel item in currentRecipeItems)
            {
                WeighingItemControlView modelControl = new WeighingItemControlView(_homeNavigationFrame, item);

                if (item.IsDone)
                {
                    modelControl.Background = Globals.LightGreen;
                    modelControl.UpdateWeighingItemBackground(Globals.LightGreen);
                }

                modelControl._viewModel.WeighingItemSelected += _viewModel_WeighingItemSelected;

                ItemList.Add(modelControl);
            }
        }

        private void SetFirstWeighingItemInList()
        {
            int nextIndex = CurrentPageIndex + 1;

            if (_recipeItemListPages.Count != nextIndex)
            {
                List<object> nextRecipeItems = _recipeItemListPages[nextIndex];

                _firstWeighingItemInList = (WeighingModel)nextRecipeItems.First();
            }
        }
        
        private void _viewModel_WeighingItemSelected(object sender, WeighingItemControlViewModel.WeighingItemSelectedEventArgs e)
        {
            OnWeighingItemSelected(new WeighingItemSelectedEventArgs(e.Weighing));
        }

        public void LoadRecipeItems(int maxItemsPerPage)
        {
            int lastModelIndex = _recipeItems.Count - 1;

            if (lastModelIndex >= 0)
            {
                RecipeItemModel lastModel = _recipeItems[lastModelIndex];
                List<object> recipeItemList = new List<object>(maxItemsPerPage);

                foreach (RecipeItemModel item in _recipeItems)
                {
                    recipeItemList.Add(item);

                    if (recipeItemList.Count == maxItemsPerPage || item == lastModel)
                    {
                        _recipeItemListPages.Add(recipeItemList);
                        recipeItemList = new List<object>(maxItemsPerPage);
                    }
                }

                LastPageIndex = _recipeItemListPages.Count - 1;

                LoadCurrentRecipeItemPage();
            }
        }

        private void LoadCurrentRecipeItemPage()
        {
            ItemList = new List<object>();

            List<object> currentRecipeItems = _recipeItemListPages[CurrentPageIndex];

            foreach (RecipeItemModel recipeItem in currentRecipeItems)
            {
                RecipeItemControlView materialButton = new RecipeItemControlView(_homeNavigationFrame, recipeItem);
                ItemList.Add(materialButton);
            }
        }

        public void UpdateWeighingItemBackground(int itemIndex, SolidColorBrush color)
        {
            ((WeighingItemControlView)ItemList[itemIndex]).Background = color;
            ((WeighingItemControlView)ItemList[itemIndex]).UpdateWeighingItemBackground(color);
        }

        #endregion

        #region Properties

        #region Title

        private string _title;

        public string Title
        {
            get
            {
                int amount = _recipeItemListPages.Sum(x => x.Count);

                return $"{_title} ({amount})";
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion
        
        #region CurrentPage

        private int _currentPage = 1;
        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                if (value != _currentPage)
                {
                    _currentPage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region MaxPages

        private int _maxPages = 1;
        public int MaxPages
        {
            get { return _maxPages; }
            set
            {
                if (value != _maxPages)
                {
                    _maxPages = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ListBoxWidth

        private int _listBoxWidth;

        public int ListBoxWidth
        {
            get { return _listBoxWidth; }
            set
            {
                if (value != _listBoxWidth)
                {
                    _listBoxWidth = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion
       
        #region ItemList

        private List<object> _itemList;

        public List<object> ItemList
        {
            get { return _itemList; }
            set
            {
                if (value != _itemList)
                {
                    _itemList = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region CurrentPageIndex
        private int _currentPageIndex;

        public int CurrentPageIndex
        {
            get
            {
                return _currentPageIndex;
            }
            set
            {
                if (value != _currentPageIndex)
                {
                    _currentPageIndex = value;
                    NotifyPropertyChanged();

                    _pageBackwardsCommand?.NotifyCanExecuteChanged();
                    _pageForwardsCommand?.NotifyCanExecuteChanged();
                    
                    if (_recipeItems.Count > 0)
                    {
                        LoadCurrentRecipeItemPage();
                    }
                    else if (_weighingItems.Count > 0)
                    {
                        LoadCurrentRecipeItemDetailPage();
                    }

                    CurrentPage = _currentPageIndex + 1;
                }
            }
        }
        #endregion

        #region LastPageIndex
        private int _lastPageIndex;

        public int LastPageIndex
        {
            get
            {
                return _lastPageIndex;
            }
            set
            {
                if (value != _lastPageIndex)
                {
                    _lastPageIndex = value;
                    NotifyPropertyChanged();
                }

                MaxPages = _lastPageIndex + 1;
            }
        }
        #endregion

        #endregion

        #region GoPageBackwards

        public ICommand GoPageBackwards => _pageBackwardsCommand = new DelegateCommand(PageBackwardsCommandExecute, CanExecutePageBackwardsCommand);

        private bool CanExecutePageBackwardsCommand()
        {
            return CurrentPageIndex != 0;
        }

        private void PageBackwardsCommandExecute()
        {
            CurrentPageIndex--;
        }

        #endregion

        #region GoPageForwards

        public ICommand GoPageForwards => _pageForwardsCommand = new DelegateCommand(PageForwardsCommandExecute, CanExecutePageForwardsCommand);

        private bool CanExecutePageForwardsCommand()
        {
            return CurrentPageIndex < LastPageIndex;
        }

        private void PageForwardsCommandExecute()
        {
            CurrentPageIndex++;
        }

        #endregion
                
        #region Event WeighingItemSelectedEventArgs

        public event EventHandler<WeighingItemSelectedEventArgs> WeighingItemSelected;

        protected virtual void OnWeighingItemSelected(WeighingItemSelectedEventArgs e)
        {
            if (WeighingItemSelected != null)
                WeighingItemSelected(this, e);
        }

        public class WeighingItemSelectedEventArgs : EventArgs
        {
            public WeighingModel Weighing { get; set; }

            public WeighingItemSelectedEventArgs(WeighingModel weighing)
            {
                Weighing = weighing;
            }
        }

        #endregion
    }
}
