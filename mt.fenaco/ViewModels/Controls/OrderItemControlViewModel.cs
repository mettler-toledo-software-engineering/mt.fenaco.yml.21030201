﻿using mt.fenaco.Logic;
using mt.fenaco.Models;
using mt.fenaco.Views;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;

namespace mt.fenaco.ViewModels.Controls
{
    public class OrderItemControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private DelegateCommand _goSelectCommand;
        private DelegateCommand _goDeleteCommand;

        public OrderItemControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, OrderModel orderItem)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Order = orderItem;
        }

        #region OrderItem
        private OrderModel _order;

        public OrderModel Order
        {
            get { return _order; }
            set
            {
                if (value != _order)
                {
                    _order = value;
                    SetVisibilityIsNew();
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region VisibilityIsNew
        private Visibility _visibilityIsNew;

        public Visibility VisibilityIsNew
        {
            get { return _visibilityIsNew; }
            set
            {
                if (value != _visibilityIsNew)
                {
                    _visibilityIsNew = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private void SetVisibilityIsNew()
        {
            VisibilityIsNew = !Order.IsNew ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion

        #region GoDelete

        public ICommand GoDelete => _goDeleteCommand = new DelegateCommand(GoDeleteCommandExecute);

        private void GoDeleteCommandExecute()
        {
            OnOrderDeleteClicked(new OrderDeleteClickedEventArgs());

            string title = Localization.Get(Localization.Key.DeleteOrder);
            string message = Localization.Get(Localization.Key.DeleteOrderMessage);

            InfoBoxView info = new InfoBoxView(title, message, MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            info.Closed += Info_Closed;
            info.Show(_parent);
        }

        private async void Info_Closed(object sender, EventArgs e)
        {
            if (((InfoBoxView)sender).ViewModel.Result == DialogResult.Yes)
            {
                OnOrderDeleting(new OrderDeletingEventArgs());

                await DatabaseExtension.DeleteOrderAndRecipeItemsAsync(Order);

                OnOrderDeleted(new OrderDeletedEventArgs());
            }
            else
            {
                OnOrderDeleteCancelled(new OrderDeleteCancelledEventArgs(Order));
            }
        }

        #endregion

        #region GoSelect

        public ICommand GoSelect => _goSelectCommand = new DelegateCommand(GoSelectCommandExecute);

        private void GoSelectCommandExecute()
        {
            OnOrderItemClicked(new OrderItemClickedEventArgs(Order));
        }

        public void SelectItem()
        {
            OnOrderItemClicked(new OrderItemClickedEventArgs(Order));
        }

        #endregion

        #region Event OrderItemClickedEventArgs

        public event EventHandler<OrderItemClickedEventArgs> OrderItemClicked;

        protected virtual void OnOrderItemClicked(OrderItemClickedEventArgs e)
        {
            if (OrderItemClicked != null)
                OrderItemClicked(this, e);
        }

        public class OrderItemClickedEventArgs : EventArgs
        {
            public OrderModel Order { get; set; }

            public OrderItemClickedEventArgs(OrderModel order)
            {
                Order = order;
            }
        }

        #endregion

        #region Event OrderDeletingEventArgs

        public event EventHandler<OrderDeletingEventArgs> OrderDeleting;

        protected virtual void OnOrderDeleting(OrderDeletingEventArgs e)
        {
            if (OrderDeleting != null)
                OrderDeleting(this, e);
        }

        public class OrderDeletingEventArgs : EventArgs
        {
            public OrderDeletingEventArgs()
            {
            }
        }

        #endregion

        #region Event OrderDeletedEventArgs

        public event EventHandler<OrderDeletedEventArgs> OrderDeleted;

        protected virtual void OnOrderDeleted(OrderDeletedEventArgs e)
        {
            if (OrderDeleted != null)
                OrderDeleted(this, e);
        }

        public class OrderDeletedEventArgs : EventArgs
        {
            public OrderDeletedEventArgs()
            {
            }
        }

        #endregion

        #region Event OrderDeleteClickedEventArgs

        public event EventHandler<OrderDeleteClickedEventArgs> OrderDeleteClicked;

        protected virtual void OnOrderDeleteClicked(OrderDeleteClickedEventArgs e)
        {
            if (OrderDeleteClicked != null)
                OrderDeleteClicked(this, e);
        }

        public class OrderDeleteClickedEventArgs : EventArgs
        {
            public OrderDeleteClickedEventArgs()
            {
            }
        }

        #endregion

        #region Event OrderDeleteCancelledEventArgs

        public event EventHandler<OrderDeleteCancelledEventArgs> OrderDeleteCancelled;

        protected virtual void OnOrderDeleteCancelled(OrderDeleteCancelledEventArgs e)
        {
            if (OrderDeleteCancelled != null)
                OrderDeleteCancelled(this, e);
        }

        public class OrderDeleteCancelledEventArgs : EventArgs
        {
            public OrderModel Order { get; set; }

            public OrderDeleteCancelledEventArgs(OrderModel order)
            {
                Order = order;
            }
        }

        #endregion
    }
}
