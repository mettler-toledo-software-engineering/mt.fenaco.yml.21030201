﻿using log4net;
using mt.fenaco.Logic;
using mt.fenaco.Models;
using mt.fenaco.Views.Controls;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Drawing;
using System;
using System.Collections.Generic;

namespace mt.fenaco.ViewModels.Controls
{
    public class WeighingItemControlViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(WeighingItemControlViewModel);

        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        public WeighingItemControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, WeighingModel weighingItem)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            WeighingItem = weighingItem;

            LoadWeighingItems();
        }

        #region Item Overview

        public void LoadWeighingItems()
        {
            DetailList = new List<WeighingItemDetailControlView>();

            List<WeighingModel> weighings = DatabaseExtension.GetWeighingDetailList(WeighingItem.Id);

            foreach (WeighingModel item in weighings)
            {
                WeighingItemDetailControlView modelControl = new WeighingItemDetailControlView(_homeNavigationFrame, item);

                if ((item.Weight != 0 && item.RecipeItem.TargetWeight != 0) || item.IsDone)
                {
                    modelControl._viewModel.WeighingItemClicked += _viewModel_WeighingItemClicked;
                    DetailList.Add(modelControl);
                }
            }
        }

        private void _viewModel_WeighingItemClicked(object sender, WeighingItemDetailControlViewModel.WeighingItemClickedEventArgs e)
        {
            try
            {
                OnWeighingItemSelected(new WeighingItemSelectedEventArgs(e.Weighing));
            }
            catch (Exception ex)
            {
                _log.ErrorEx("selecting weighing item failed", _sourceClass, ex);
            }
        }

        #endregion

        #region Background
        private SolidColorBrush _background;

        public SolidColorBrush Background
        {
            get { return _background; }
            set
            {
                if (value != _background)
                {
                    _background = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public void UpdateWeighingItemBackground(SolidColorBrush color)
        {
            foreach (var item in DetailList)
            {
                item.Background = color;
            }
        }
        #endregion

        #region WeighingModel
        private WeighingModel _weighingItem;

        public WeighingModel WeighingItem
        {
            get { return _weighingItem; }
            set
            {
                if (value != _weighingItem)
                {
                    _weighingItem = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
        
        #region DetailList

        private List<WeighingItemDetailControlView> _detailList;

        public List<WeighingItemDetailControlView> DetailList
        {
            get { return _detailList; }
            set
            {
                if (value != _detailList)
                {
                    _detailList = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region RecipeItem
        private RecipeItemModel _recipeItem;

        public RecipeItemModel RecipeItem
        {
            get { return _recipeItem; }
            set
            {
                if (value != _recipeItem)
                {
                    _recipeItem = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Event WeighingItemSelectedEventArgs

        public event EventHandler<WeighingItemSelectedEventArgs> WeighingItemSelected;

        protected virtual void OnWeighingItemSelected(WeighingItemSelectedEventArgs e)
        {
            if (WeighingItemSelected != null)
                WeighingItemSelected(this, e);
        }

        public class WeighingItemSelectedEventArgs : EventArgs
        {
            public WeighingModel Weighing { get; set; }

            public WeighingItemSelectedEventArgs(WeighingModel weighing)
            {
                Weighing = weighing;
            }
        }

        #endregion
    }
}
