﻿using mt.fenaco.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Drawing;

namespace mt.fenaco.ViewModels.Controls
{
    public class RecipeItemControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        public RecipeItemControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, RecipeItemModel recipeItem)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            RecipeItem = recipeItem;
        }

        #region Background
        private SolidColorBrush _background;

        public SolidColorBrush Background
        {
            get { return _background; }
            set
            {
                if (value != _background)
                {
                    _background = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region RecipeItem
        private RecipeItemModel _recipeItem;

        public RecipeItemModel RecipeItem
        {
            get { return _recipeItem; }
            set
            {
                if (value != _recipeItem)
                {
                    _recipeItem = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion
    }
}
