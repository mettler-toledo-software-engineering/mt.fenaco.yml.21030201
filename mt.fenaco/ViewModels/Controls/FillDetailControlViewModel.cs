﻿using mt.fenaco.Infrastructure;
using mt.fenaco.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Drawing;

namespace mt.fenaco.ViewModels.Controls
{
    public class FillDetailControlViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        public FillDetailControlViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, WeighingModel weighing, double newTarget)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _newTargetWeight = newTarget;

            Background = Globals.LightBlue;
            WeighingItem = weighing;
        }

        #region Background

        private SolidColorBrush _background;

        public SolidColorBrush Background
        {
            get
            {
                return _background;
            }
            set
            {
                if (value != _background)
                {
                    _background = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region WeighingItem

        private WeighingModel _weighingItem;

        public WeighingModel WeighingItem
        {
            get
            {
                return _weighingItem;
            }
            set
            {
                if (value != _weighingItem)
                {
                    _weighingItem = value;
                    NotifyPropertyChanged();
                    ShowOriginPlace();
                }
            }
        }

        private void ShowOriginPlace()
        {
            VisibilityOriginPlace = WeighingItem.RecipeItem.OriginPlace != "" ? Visibility.Visible : Visibility.Collapsed;
        }

        #endregion

        #region NewTargetWeightToString

        private double _newTargetWeight;

        public string NewTargetWeightToString
        {
            get
            {
                return $"{_newTargetWeight} {Globals.GetCurrentScaleUnit()}";
            }
        }

        #endregion

        #region TargetWeightToString
        
        public string TargetWeightToString
        {
            get
            {
                return $"(max. {WeighingItem.TargetWeightToString})";
            }
        }

        #endregion

        #region VisibilityOriginPlace

        private Visibility _visibilityOriginPlace;

        public Visibility VisibilityOriginPlace
        {
            get
            {
                return _visibilityOriginPlace;
            }
            set
            {
                if (value != _visibilityOriginPlace)
                {
                    _visibilityOriginPlace = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion
    }
}
