﻿using log4net;
using mt.fenaco.Infrastructure;
using mt.fenaco.Logic;
using mt.fenaco.Models;
using mt.fenaco.Views;
using mt.fenaco.Views.Controls;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mt.fenaco.ViewModels
{
    public class OrderSelectionViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(OrderSelectionViewModel);

        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;
        
        private DelegateCommand _goBackCommand;
        private DelegateCommand _goReadRecipesCommand;
        private DelegateCommand _goStartFillCommand;

        private InfoBoxView _infoBoxDelete;

        private OrderModel _orderToComplete;
        private OpenOrderModel _openOrderToComplete;

        private bool _isLoading = false;

        public OrderSelectionViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            
            ResetValues();
        }

        #region Load Items

        public void LoadItems()
        {
            ItemList = new List<OrderItemControlView>();
            ItemList.Clear();

            foreach (OrderModel order in DatabaseExtension.Orders)
            {
                order.IsNew = !OrderNotFinished(order);
                OrderItemControlView modelControl = new OrderItemControlView(_homeNavigationFrame, order);
                modelControl.ViewModel.OrderItemClicked += ViewModel_OrderItemClicked;
                modelControl.ViewModel.OrderDeleting += ViewModel_OrderDeleting;
                modelControl.ViewModel.OrderDeleteClicked += ViewModel_OrderDeleteClicked;
                modelControl.ViewModel.OrderDeleteCancelled += ViewModel_OrderDeleteCancelled;
                modelControl.ViewModel.OrderDeleted += ViewModel_OrderDeleted;
                ItemList.Add(modelControl);
            }

            ItemCounter = ItemList.Count;

            NotifyCanExecuteChanged();
        }

        public async void ResetValues()
        {
            await DatabaseExtension.GetAllOrdersFromDBAsync();

            SelectedOrder = ItemList.Count == 1 ? DatabaseExtension.Orders.FirstOrDefault() : null;

            LoadItems();
            ResetProgressBar();
            NotifyCanExecuteChanged();
        }

        private bool OrderNotFinished(OrderModel order)
        {
            var result = DatabaseExtension.OpenOrders.Where(i => i.OrderId == order.Id).FirstOrDefault();

            return result != null;
        }

        private void ViewModel_OrderItemClicked(object sender, Controls.OrderItemControlViewModel.OrderItemClickedEventArgs e)
        {
            try
            {
                SelectedOrder = e.Order;
                NotifyCanExecuteChanged();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }

        #endregion

        #region CheckForFinishedRecipes
        
        public async Task CheckForFinishedRecipes()
        {
            int counter = 0;
            List<int> openOrderIds = DatabaseExtension.OpenOrders.Select(item => item.Id).ToList();

            foreach (var openOrderId in openOrderIds)
            {
                _openOrderToComplete = DatabaseExtension.OpenOrders.FirstOrDefault(item => item.Id == openOrderId);
                _orderToComplete = DatabaseExtension.Orders.FirstOrDefault(item => item.Id == _openOrderToComplete.OrderId);

                if (_openOrderToComplete != null && _orderToComplete == null)
                {
                    await DatabaseExtension.DeleteOpenOrderAndWeighingsAsync(_openOrderToComplete);
                    continue;
                }

                var list = DatabaseExtension.Weighings.Where(item => item.OpenOrderId == openOrderId && !item.IsDone).ToList();

                if (list.Count == 0)
                {
                    ShowStayOnPageQuestion();
                    counter++;
                }
            }

            if (counter > 0)
            {
                ResetValues();
            }
        }

        private void WriteCsvFile()
        {
            CsvFileExtension.WriteCsvFile(_parent, _orderToComplete, _openOrderToComplete);
        }

        private void ShowStayOnPageQuestion()
        {
            string title = Localization.Get(Localization.Key.CompleteRecipeTitle);
            string message = Localization.Get(Localization.Key.CompleteRecipeQuestion);
            message += $"\n\n {Localization.Get(Localization.Key.RecipeName)}:'{_orderToComplete?.Name}'";

            InfoBoxView question = new InfoBoxView(title, message, MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            question.ViewModel.Closed += ViewModel_Closed;
            question.Show(_parent);
        }

        private void ViewModel_Closed(object sender, InfoBoxClosedEventArgs e)
        {
            if (e.DialogResult == DialogResult.Yes)
            {
                _log.InfoEx($"write csv file for order '{_orderToComplete?.Name}'", _sourceClass);

                WriteCsvFile();

                return;
            }

            _log.InfoEx($"do not write csv file for order '{_orderToComplete?.Name}'", _sourceClass);
        }

        #endregion

        #region Delete event

        private void ViewModel_OrderDeleting(object sender, Controls.OrderItemControlViewModel.OrderDeletingEventArgs e)
        {
            ShowInfoBoxDelete();
        }

        private void ViewModel_OrderDeleted(object sender, Controls.OrderItemControlViewModel.OrderDeletedEventArgs e)
        {
            ResetValues();
            LoadItems();

            CloseInfoBoxDelete();
        }

        private void ViewModel_OrderDeleteCancelled(object sender, Controls.OrderItemControlViewModel.OrderDeleteCancelledEventArgs e)
        {
            ResetValues();
            SelectedOrder = e.Order;
            NotifyCanExecuteChanged();
        }

        private void ViewModel_OrderDeleteClicked(object sender, Controls.OrderItemControlViewModel.OrderDeleteClickedEventArgs e)
        {
            _isLoading = false;
            SelectedOrder = null;
            NotifyCanExecuteChanged();
        }

        private void ShowInfoBoxDelete()
        {
            string title = Localization.Get(Localization.Key.DeleteRecipeTitle);
            string message = Localization.Get(Localization.Key.DeleteRecipeMessage);

            _infoBoxDelete = new InfoBoxView(title, message, MessageBoxIcon.Information, MessageBoxButtons.OK, false);
            _infoBoxDelete.Show(_parent);
        }

        private void CloseInfoBoxDelete()
        {
            _infoBoxDelete.Close();
        }

        #endregion

        #region Properties

        #region ItemCounter

        private int _itemCounter;

        public int ItemCounter
        {
            get { return _itemCounter; }
            set
            {
                if (value != _itemCounter)
                {
                    _itemCounter = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ItemList

        private List<OrderItemControlView> _itemList = new List<OrderItemControlView>();

        public List<OrderItemControlView> ItemList
        {
            get { return _itemList; }
            set
            {
                if (value != _itemList)
                {
                    _itemList = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ProgressBarMessage

        private string _progressBarMessage;

        public string ProgressBarMessage
        {
            get { return _progressBarMessage; }
            set
            {
                if (value != _progressBarMessage)
                {
                    _progressBarMessage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ProgressBarMaximumValue

        private int _progressBarMaximumValue;

        public int ProgressBarMaximumValue
        {
            get { return _progressBarMaximumValue; }
            set
            {
                if (value != _progressBarMaximumValue)
                {
                    _progressBarMaximumValue = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ProgressBarValue

        private int _progressBarValue;

        public int ProgressBarValue
        {
            get { return _progressBarValue; }
            set
            {
                if (value != _progressBarValue)
                {
                    _progressBarValue = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ProgressBarMessageisibility

        private Visibility _progressBarMessageVisibility;

        public Visibility ProgressBarMessageVisibility
        {
            get { return _progressBarMessageVisibility; }
            set
            {
                if (value != _progressBarMessageVisibility)
                {
                    _progressBarMessageVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ProgressBarValueVisibility

        private Visibility _progressBarVisibility;

        public Visibility ProgressBarVisibility
        {
            get { return _progressBarVisibility; }
            set
            {
                if (value != _progressBarVisibility)
                {
                    _progressBarVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region SelectedItem

        private OrderModel _selectedItem;

        public OrderModel SelectedOrder
        {
            get { return _selectedItem; }
            set
            {
                if (value != _selectedItem)
                {
                    _selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

        #region KeyHandling
        
        #region GoBack

        public ICommand GoBack => _goBackCommand = new DelegateCommand(GoBackCommandExecute, CanExecuteGoBackRecipesCommand);

        private void GoBackCommandExecute()
        {
            _homeNavigationFrame.Back();
        }

        private bool CanExecuteGoBackRecipesCommand()
        {
            return !_isLoading;
        }

        #endregion

        #region GoReadRecipes

        public ICommand GoReadRecipes => _goReadRecipesCommand = new DelegateCommand(GoReadRecipesCommandExecute, CanExecuteGoReadRecipesCommand);

        private async void GoReadRecipesCommandExecute()
        {
            SelectedOrder = null;

            _log.InfoEx("Rezept einlesen", _sourceClass);

            ResetProgressBar(false);
            NotifyCanExecuteChanged();

            bool res = await Globals.GetAllOrdersFromUSBAndNetworkAsync(this);
            
            if (res)
                LoadItems();
            else
            {
                string title = Localization.Get(Localization.Key.NoRecipeFound);
                string message = Localization.Get(Localization.Key.NoRecipeFoundMessage);

                InfoBoxView info = new InfoBoxView(title, message, MessageBoxIcon.Information, MessageBoxButtons.OK);
                info.Show(_parent);
            }

            ResetProgressBar();
        }

        private bool CanExecuteGoReadRecipesCommand()
        {
            return !_isLoading;
        }

        private void ResetProgressBar(bool reset = true)
        {
            _isLoading = !reset;
            ProgressBarVisibility = reset ? Visibility.Collapsed : Visibility.Visible;
            ProgressBarMessageVisibility = reset ? Visibility.Collapsed : Visibility.Visible;

            NotifyCanExecuteChanged();
        }

        #endregion

        #region GoStartFill

        public ICommand GoStartFill => _goStartFillCommand = new DelegateCommand(GoStartFillCommandExecute, CanExecuteGoStartFillCommand);

        private void GoStartFillCommandExecute()
        {
            Process.OpenOrder = DatabaseExtension.OpenOrders.Where(item => item.OrderId == SelectedOrder.Id).FirstOrDefault();

            _homeNavigationFrame.NavigateTo(new MainProcessView(_homeNavigationFrame, SelectedOrder), TransitionAnimation.LeftToRight);
        }

        private bool CanExecuteGoStartFillCommand()
        {
            return SelectedOrder != null;
        }

        #endregion

        #endregion

        #region Helper Methods

        private void NotifyCanExecuteChanged()
        {
            _goBackCommand?.NotifyCanExecuteChanged();
            _goReadRecipesCommand?.NotifyCanExecuteChanged();
            _goStartFillCommand?.NotifyCanExecuteChanged();
        }

        #endregion
    }
}
