﻿using log4net;
using mt.fenaco.Infrastructure;
using mt.fenaco.Logic;
using mt.fenaco.Logic.BarcodeReader;
using mt.fenaco.Models;
using mt.fenaco.Views;
using mt.fenaco.Views.Controls;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Controls.DeltaTrac;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static mt.fenaco.Infrastructure.Globals;

namespace mt.fenaco.ViewModels
{
    public class FillProcessViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(FillProcessViewModel);

        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;

        private DelegateCommand _goCancelCommand;
        private DelegateCommand _goTareCommand;
        private DelegateCommand _goPresetTareCommand;
        private DelegateCommand _goClearTareCommand;
        private DelegateCommand _goEditWeighingCommand;
        private DelegateCommand _goDeleteWeighingCommand;
        private DelegateCommand _goSkipCommand;
        private DelegateCommand _goSplitCommand;
        private DelegateCommand _goNextCommand;
        private IBarcodeReader _barcodeReader;

        private int _itemIndex = 0;
        private int _currentListIndex = 0;
        private string _title;
        private IScale _scale;
        private WeightInformation _weightInformation = new WeightInformation();

        private List<WeighingModel> _weighingItems = new List<WeighingModel>();

        private List<WeighingModel> _grassWeighingItems;
        private List<WeighingModel> _flowerWeighingItems;

        private ProcessState _newState;
        private ProcessState _oldState;

        private bool _flowerWeighingsComplete;
        private bool _grassWeighingsComplete;

        //private bool _hasContainer;

        public FillProcessViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            
            _flowerWeighingsComplete = false;
            _grassWeighingsComplete = false;
        }
        
        #region Initialize and Update Fields

        public void Initialize()
        {
            _newState = ProcessState.Initialize;
            _log.InfoEx($"new state: {_newState}", _sourceClass);

            //DoTare();
            RegisterScaleEvent();
            RegisterBarcodeReader();
            RegisterEventsForViewModel();
        }

        private void _viewModel_WeighingItemSelected(object sender, Controls.RecipeControlViewModel.WeighingItemSelectedEventArgs e)
        {
            SelectedWeighingItemDetail = e.Weighing;
            NotifyCanExecuteChanged();
        }

        private void _scale_NewWeightInDisplayUnit(WeightInformation weight)
        {
            _weightInformation = weight;

            CheckStateEngine();            
            NotifyCanExecuteChanged();
        }

        private void CheckStateEngine()
        {

            switch (_newState)
            {
                case ProcessState.Initialize:
                    GetWeighingsFromDB();

                    _flowerWeighingsComplete = !HasOpenWeighings(_flowerWeighingItems);
                    _grassWeighingsComplete = !HasOpenWeighings(_grassWeighingItems);

                    _newState = ProcessState.StartFillFlower;
                    _log.InfoEx($"new state: {_newState}", _sourceClass);

                    break;
                case ProcessState.Filling:
                    _flowerWeighingsComplete = !HasOpenWeighings(_flowerWeighingItems);
                    _grassWeighingsComplete = !HasOpenWeighings(_grassWeighingItems);

                    if (_flowerWeighingsComplete && _grassWeighingsComplete)
                    {
                        _newState = ProcessState.Completed;
                        _log.InfoEx($"new state: {_newState}", _sourceClass);
                    }

                    //bedingungen ergänzt damit nachdem filling gestartet wurde auch noch auf andere states gewechselt werden kann
                    //bedingungen sind so formuliert, dass von grass auf flower und von flower auf grass gewechselt werden kann
                    if (_flowerWeighingsComplete && _grassWeighingsComplete == false && _oldState == ProcessState.StartFillFlower)
                    {
                        _newState = ProcessState.StartFillGrass;
                    }
                    if (_grassWeighingsComplete && _flowerWeighingsComplete == false && _oldState == ProcessState.StartFillGrass)
                    {
                        _newState = ProcessState.StartFillFlower;
                    }

                    break;
                case ProcessState.StartFillFlower:
                    _oldState = _newState;
                    _newState = _flowerWeighingItems.Count == 0 ? ProcessState.StartFillGrass : ProcessState.Filling;
                    //_newState = _flowerWeighingItems.Count == 0 || _flowerWeighingsComplete ? ProcessState.StartFillGrass : ProcessState.Filling;
                    _log.InfoEx($"new state: {_newState}", _sourceClass);

                    GetWeighingsFromDB();

                    if (HasOpenWeighings(_flowerWeighingItems))
                    {
                        StartFillRecipe(Localization.Get(Localization.Key.Flower), _flowerWeighingItems);
                    }

                    break;
                case ProcessState.StartFillGrass:
                    _oldState = _newState;
                    _newState = _grassWeighingItems.Count == 0  ? ProcessState.StartFillFlower : ProcessState.Filling;
                   // _newState = _grassWeighingItems.Count == 0 || _grassWeighingsComplete ? ProcessState.StartFillFlower : ProcessState.Filling;
                    _log.InfoEx($"new state: {_newState}", _sourceClass);

                    GetWeighingsFromDB();

                    if (HasOpenWeighings(_grassWeighingItems))
                    {
                        StartFillRecipe(Localization.Get(Localization.Key.Grass), _grassWeighingItems);
                    }
                    break;
                case ProcessState.LastItemReached:
                    _newState = ProcessState.Idle;
                    _log.InfoEx($"new state: {_newState}", _sourceClass);

                    ShowStayOnPageQuestion();

                    break;
                case ProcessState.Completed:
                    _newState = ProcessState.Idle;

                    FillOverviewView overview = new FillOverviewView(_homeNavigationFrame, _flowerWeighingItems, _grassWeighingItems);
                    overview._viewModel.Cancelled += _viewModel_Cancelled;
                    overview._viewModel.RecipeCompleted += _viewModel_RecipeCompleted;

                    _homeNavigationFrame.NavigateTo(overview, TransitionAnimation.LeftToRight);                    

                    break;
                case ProcessState.Idle:
                    break;
                default:
                    break;
            }
        }

        private void _viewModel_Cancelled(object sender, FillOverviewViewModel.CancelledEventArgs e)
        {
            _homeNavigationFrame.Back();
            OnCancelled(new CancelledEventArgs());
        }

        private void _viewModel_RecipeCompleted(object sender, FillOverviewViewModel.RecipeCompletedEventArgs e)
        {
            _homeNavigationFrame.Back();
            OnRecipeCompleted(new RecipeCompletedEventArgs());
        }

        private void ShowStayOnPageQuestion()
        {
            string title = Localization.Get(Localization.Key.Next);
            string message = Localization.Get(Localization.Key.SwitchToNextComponents);

            InfoBoxView question = new InfoBoxView(title, message, MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            question.ViewModel.Closed += ViewModel_Closed;
            question.Show(_parent);
        }

        private void ViewModel_Closed(object sender, InfoBoxClosedEventArgs e)
        {
            if (e.DialogResult == DialogResult.Yes)
            {
                _log.InfoEx($"go to next components", _sourceClass);

                _newState = _oldState == ProcessState.StartFillFlower ? ProcessState.StartFillGrass : ProcessState.StartFillFlower;
                //CalculatedPageIndex = 0;
                _log.InfoEx($"new state: {_newState}", _sourceClass);

                return;
            }

            _log.InfoEx($"stay on this page", _sourceClass);

            _newState = _oldState;
            _log.InfoEx($"new state: {_newState}", _sourceClass);
        }

        private void ResetContent()
        {
            UpdateCurrentWeighingItem();
            ColorPreviousItems();
            ChangeBackgroundForSelectedItem();
        }

        private List<WeighingModel> GetCurrentWeighingsByType(RecipeType type)
        {
            return DatabaseExtension.GetWeighingsByType(Process.Order, type);
        }
        private bool HasOpenWeighings(List<WeighingModel> weighingItems)
        {
            return weighingItems.Where(i => !i.IsDone).Count() > 0;
        }

        private void StartFillRecipe(string title, List<WeighingModel> weighingItems)
        {
            _title = title;
            _weighingItems = weighingItems.OrderBy(i => i.IsDone).ThenBy(n => n.RecipeItem.NameBot).ToList();
            _itemIndex = 0;
            _currentListIndex = 0;
            CalculatedPageIndex = 0;

            UpdateCurrentRecipeView();
            ResetContent();

            SetDeltaTracValues();
        }

        private void GetWeighingsFromDB()
        {
            _flowerWeighingItems = GetCurrentWeighingsByType(RecipeType.Flower);
            _grassWeighingItems = GetCurrentWeighingsByType(RecipeType.Grass);
        }

        private void UpdateCurrentWeighingItem()
        {
            _log.InfoEx($"update current recipe item", _sourceClass);

            WeighingItem = _weighingItems[_itemIndex];

            _log.InfoEx($"new recipe item: {WeighingItem.ToString()}", _sourceClass);
        }

        private void ColorPreviousItems()
        {
            if (WeighingItem.IsDone)
            {
                ChangeBackgroundForCompletedItem();
            }
        }
        private void UpdateFillDetail(double newTarget)
        {
            FillDetail = new FillDetailControlView(_homeNavigationFrame, WeighingItem, newTarget);
        }

        private void NotifyCanExecuteChanged()
        {
            _goCancelCommand?.NotifyCanExecuteChanged();
            _goEditWeighingCommand?.NotifyCanExecuteChanged();
            _goDeleteWeighingCommand?.NotifyCanExecuteChanged();
            _goSkipCommand?.NotifyCanExecuteChanged();
            _goSplitCommand?.NotifyCanExecuteChanged();
            _goNextCommand?.NotifyCanExecuteChanged();
        }

        #endregion

        #region Scale

        #region Register Scale Event

        private void RegisterScaleEvent()
        {
            if (_scale == null)
            {
                _scale = GetCurrentScale();
            }

            _log.InfoEx($"set new changed weight event", _sourceClass);
            //todo warum beide events
            _scale.NewWeightInDisplayUnit += _scale_NewWeightInDisplayUnit;
            _scale.NewChangedWeightInDisplayUnit += _scale_NewChangedWeightInDisplayUnit;
        }

        private void _scale_NewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            //TryExecuteTareAndClaereTare(weight);
        }

        private void TryExecuteTareAndClaereTare(WeightInformation weight)
        {
            //if (!_hasContainer && weight.NetStable && weight.NetWeight >= Globals.Config.MinWeightContainer)
            //{
            //    GoTareCommandExecute();
            //}

            //if (_weightInformation.NetWeight < 0 && _hasContainer)
            //{
            //    GoClearTareCommandExecute();
            //}
        }

        #endregion

        #region Unregister Scale Event

        public void UnregisterScaleEvent()
        {
            _log.InfoEx($"scale event on viewmodel NULL", _sourceClass);

            if (_scale != null)
            {
                _scale.NewWeightInDisplayUnit -= _scale_NewWeightInDisplayUnit;
                _scale.NewChangedWeightInDisplayUnit -= _scale_NewWeightInDisplayUnit;
            }

            _scale = null;
        }

        #endregion

        #endregion

        #region Barcode Scanner

        #region RegisterEvents

        private void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }

        private void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                _log.InfoEx($"barcode reader on viewmodel ready", _sourceClass);
                return;
            }

            _log.Error($"barcode reader on viewmodel NULL");
        }

        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            _log.InfoEx($"barcode reader on viewmodel data received {e.Data}", _sourceClass);

            if (e.ArticleName == WeighingItem.RecipeItem.NameBot)
            {
                SaveCurrentInfosAsync(e.Charge, e.ArticleNumber, _weightInformation.NetWeight, true).ContinueWith(SaveCurrentInfosAsyncCompleted);
            }
            else
            {
                string title = Localization.Get(Localization.Key.Scannfailed);
                string message = Localization.Get(Localization.Key.WrongItemScanned);

                InfoBoxView info = new InfoBoxView(title, message, MessageBoxIcon.Error, MessageBoxButtons.OK, false);
                info.Show(_parent);
                Thread.Sleep(3500);
                info.Close();

                _log.ErrorEx($"wrong item scanned!", _sourceClass);
            }
        }

        #endregion

        #region UnregisterEvents
        public void UnregisterEventsForViewModel()
        {
            _log.InfoEx($"barcode reader on viewmodel NULL", _sourceClass);

            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
            _barcodeReader = null;
        }

        #endregion

        #endregion
        
        #region SetDeltaTracValues

        private void SetDeltaTracValues()
        {
            _log.InfoEx($"set deltatrac values", _sourceClass);
            
            UpdateCurrentWeighingItem();

            double target = CalculateNewTarget();

            UpdateFillDetail(target);

            _log.InfoEx($"new target: {target}", _sourceClass);

            //Change 21.06. request 10% toleranz für DT
            //const double lowTol = 0;
            double lowTol = target * LowerToleranceFactor;
            var upTol = target * UpperToleranceFactor;

            _log.InfoEx($"upper tolerance: {upTol}", _sourceClass);

            Target = target.ToString();
            LowerTolerance = lowTol.ToString();
            UpperTolerance = upTol.ToString();
        }

        private double GetTargetWeight()
        {
            return WeighingItem.TargetWeight;
        }

        #endregion

        #region Properties

        #region Title
        
        public string RecipeTitle
        {
            get
            {
                return Process.Order.Name;
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region CalculatedPageIndex

        private int _calculatedPageIndex;
        public int CalculatedPageIndex
        {
            get
            {
                return _calculatedPageIndex;
            }
            set
            {
                if (value != _calculatedPageIndex)
                {
                    _calculatedPageIndex = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region RecipeView

        private RecipeControlView _RecipeView;

        public RecipeControlView RecipeView
        {
            get
            {
                return _RecipeView;
            }
            set
            {
                _RecipeView = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region FillDetail

        private FillDetailControlView _fillDetail;

        public FillDetailControlView FillDetail
        {
            get
            {
                return _fillDetail;
            }
            set
            {
                _fillDetail = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region WeighingItem

        private WeighingModel _weighingItem;
        public WeighingModel WeighingItem
        {
            get
            {
                return _weighingItem;
            }
            set
            {
                if (value != _weighingItem)
                {
                    _weighingItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion
        
        #region SelectedWeighingItemDetail

        private WeighingModel _selectedWeighingItemDetail;
        public WeighingModel SelectedWeighingItemDetail
        {
            get
            {
                return _selectedWeighingItemDetail;
            }
            set
            {
                if (value != _selectedWeighingItemDetail)
                {
                    _selectedWeighingItemDetail = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region DeltaTrac

        #region Target
        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public string Target
        {
            get { return _target; }
            set
            {
                if (_target != value)
                {
                    _target = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _target = "3";
        #endregion

        #region LowerTolerance
        /// <summary>
        /// Gets or sets the lower tolerance.
        /// </summary>
        /// <value>
        /// The lower tolerance.
        /// </value>
        public string LowerTolerance
        {
            get { return _lowerTolerance; }
            set
            {
                // ReSharper disable once RedundantCheckBeforeAssignment
                if (_lowerTolerance != value)
                {
                    _lowerTolerance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _lowerTolerance = "-1";
        #endregion

        #region UpperTolerance
        /// <summary>
        /// Gets or sets the upper tolerance.
        /// </summary>
        /// <value>
        /// The upper tolerance.
        /// </value>
        public string UpperTolerance
        {
            get { return _upperTolerance; }
            set
            {
                if (_upperTolerance != value)
                {
                    _upperTolerance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _upperTolerance = "2";
        #endregion

        #region Appereance
        /// <summary>
        /// Gets or sets the appearance.
        /// </summary>
        /// <value>
        /// The appearance.
        /// </value>
        public DeltaTracAppearance Appearance
        {
            get { return _appearance; }
            set
            {
                if (_appearance != value)
                {
                    _appearance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private DeltaTracAppearance _appearance = DeltaTracAppearance.BarGraph;
        #endregion

        #endregion DeltaTrac

        #endregion

        #region Key Commands

        #region GoCancel
        public ICommand GoCancel => _goCancelCommand = new DelegateCommand(GoBackCommandExecute);

        private void GoBackCommandExecute()
        {
            string title = Localization.Get(Localization.Key.CancelWeighing);
            string message = Localization.Get(Localization.Key.CancelWeighingMessage);

            InfoBoxView info = new InfoBoxView(title, message, MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            info.Closed += Info_Closed;
            info.Show(_parent);
        }

        private void Info_Closed(object sender, EventArgs e)
        {
            if (((InfoBoxView)sender).ViewModel.Result == DialogResult.Yes)
            {
                _log.InfoEx($"cancel", _sourceClass);

                _homeNavigationFrame.Back();
                OnCancelled(new CancelledEventArgs());
            }
        }

        #endregion

        #region Tare
        public ICommand GoTare => _goTareCommand = new DelegateCommand(GoTareCommandExecute);

        private void GoTareCommandExecute()
        {
            //_hasContainer = true;
            Globals.DoTare();
        }

        #endregion

        #region TareWithWeight
        public ICommand GoPresetTare => _goPresetTareCommand = new DelegateCommand(GoPresetTareCommandExecute);

        private void GoPresetTareCommandExecute()
        {
            //_hasContainer = true;
            Globals.DoPresetTare(_weightInformation.GrossWeightString, _weightInformation.Unit);
        }

        #endregion

        #region ClearTare

        public ICommand GoClearTare => _goClearTareCommand = new DelegateCommand(GoClearTareCommandExecute);

        private void GoClearTareCommandExecute()
        {
            //_hasContainer = false;
            Globals.DoClearTare();
        }

        #endregion

        #region GoEditWeighing

        public ICommand GoEditWeighing => _goEditWeighingCommand = new DelegateCommand(GoEditWeighingCommandExecute, CanExecuteGoEditWeighingCommand);

        private void GoEditWeighingCommandExecute()
        {
            UnregisterEventsForViewModel();

            AddManualInfosView editWindow = new AddManualInfosView(_homeNavigationFrame, SelectedWeighingItemDetail, WeighingItem.Weight, false);
            editWindow._viewModel.Saved += _viewModel_WeighingItemUpdated;
            editWindow._viewModel.Closed += _viewModel_Closed;
            editWindow.Show(_parent);
        }

        private void _viewModel_WeighingItemUpdated(object sender, AddManualInfosViewModel.SavedEventArgs e)
        {
            SelectedWeighingItemDetail.Charge = e.Charge;
            SelectedWeighingItemDetail.ProductNr = e.ProductNr;
            SelectedWeighingItemDetail.Weight = e.Weight;

            UpdateWeighingItemInDBAsync().ContinueWith(CurrentInfosSavedInDB);
        }

        private bool CanExecuteGoEditWeighingCommand()
        {
            return SelectedWeighingItemDetail != null;
        }

        private void ResetSelectedWeighingItemDetail()
        {
            SelectedWeighingItemDetail = null;
        }

        #endregion

        #region GoDeleteWeighing

        public ICommand GoDeleteWeighing => _goDeleteWeighingCommand = new DelegateCommand(GoDeleteWeighingCommandExecute, CanDeleteGoEditWeighingCommand);

        private void GoDeleteWeighingCommandExecute()
        {
            string title = Localization.Get(Localization.Key.DeleteWeighing);
            string message = Localization.Get(Localization.Key.DeleteWeighingMessage);

            InfoBoxView infoDeleteWeighing = new InfoBoxView(title, message, MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            infoDeleteWeighing.Closed += InfoDeleteWeighing_Closed;
            infoDeleteWeighing.Show(_parent);
        }

        private async void InfoDeleteWeighing_Closed(object sender, EventArgs e)
        {
            if (((InfoBoxView)sender).ViewModel.Result == DialogResult.Yes)
            {
                ChangeBackgroundForUncompletedItem();

                WeighingModel parent = DatabaseExtension.Weighings.Where(item => item.Id == SelectedWeighingItemDetail.ParentId).FirstOrDefault();

                if (parent != null)
                {
                    parent.IsDone = false;
                    await DatabaseExtension.DeleteWeighingAsync(SelectedWeighingItemDetail);
                }
                else
                {
                    parent = SelectedWeighingItemDetail;
                    parent.IsDone = false;
                    parent.Charge = null;
                    parent.ProductNr = null;
                    parent.Weight = 0;
                }

                await DatabaseExtension.UpdateWeighingInDBAsync(parent);

                ResetSelectedWeighingItemDetail();
                UpdateCurrentRecipeView();
                NotifyCanExecuteChanged();
            }
        }

        private bool CanDeleteGoEditWeighingCommand()
        {
            return SelectedWeighingItemDetail != null;
        }

        #endregion

        #region GoSkip
        public ICommand GoSkip => _goSkipCommand = new DelegateCommand(GoSkipCommandExecute);

        private void GoSkipCommandExecute()
        {
            ResetSelectedWeighingItemDetail();
            GoToNextItem(true);
        }

        #endregion

        #region GoSplit
        public ICommand GoSplit => _goSplitCommand = new DelegateCommand(GoSplitCommandExecute, CanExecuteGoSplitCommand);

        private void GoSplitCommandExecute()
        {
            UnregisterEventsForViewModel();

            AddManualInfosView view = new AddManualInfosView(_homeNavigationFrame, WeighingItem, _weightInformation.NetWeight);
            view._viewModel.Saved += _viewModel_SavedSplit;
            view._viewModel.Closed += _viewModel_Closed;
            view.Show(_parent);
        }

        private bool CanExecuteGoSplitCommand()
        {
            //return _hasContainer;
            return true;
        }

        private void _viewModel_Closed(object sender, AddManualInfosViewModel.ClosedEventArgs e)
        {
            ResetSelectedWeighingItemDetail();
            NotifyCanExecuteChanged();
            RegisterBarcodeReader();
            RegisterEventsForViewModel();
        }

        private void _viewModel_SavedSplit(object sender, AddManualInfosViewModel.SavedEventArgs e)
        {
            SaveCurrentInfosAsync(e.Charge, e.ProductNr, e.Weight, false).ContinueWith(Split_CurrentInfosSavedInDB);
        }

        private void Split_CurrentInfosSavedInDB(Task task)
        {
            if (task.Exception == null)
            {
                GoPresetTareCommandExecute();
                CurrentInfosSavedInDB(task);
            }
            else
            {
                _log.ErrorEx($"save current infos failed", _sourceClass);
            }
        }

        private void CurrentInfosSavedInDB(Task task)
        {
            if (task.Exception == null)
            {
                _log.InfoEx($"current infos saved", _sourceClass);
                
                SetDeltaTracValues();

                UpdateCurrentRecipeView();
                ChangeBackgroundForSelectedItem();
            }
            else
            {
                _log.ErrorEx($"save current infos failed", _sourceClass);
            }
        }

        private double CalculateNewTarget()
        {
            List<WeighingModel> weighings = DatabaseExtension.GetWeighingsByRecipeItemId(WeighingItem.RecipeItemId);
            double newTarget = WeighingItem.TargetWeight - weighings.Where(i=> i.ParentId == WeighingItem.Id).Sum(x => x.Weight);
            
            NotifyCanExecuteChanged();

            return newTarget;
        }

        #endregion

        #region GoNext
        private void GoNextCommandExecute()
        {
            UnregisterEventsForViewModel();

            AddManualInfosView view = new AddManualInfosView(_homeNavigationFrame, WeighingItem, _weightInformation.NetWeight);
            view._viewModel.Saved += _viewModel_Saved;
            view._viewModel.Closed += _viewModel_Closed1;
            view.Show(_parent);
        }

        private void _viewModel_Closed1(object sender, AddManualInfosViewModel.ClosedEventArgs e)
        {
            ResetSelectedWeighingItemDetail();
            NotifyCanExecuteChanged();
            RegisterBarcodeReader();
            RegisterEventsForViewModel();
        }

        private void _viewModel_Saved(object sender, AddManualInfosViewModel.SavedEventArgs e)
        {
            SaveCurrentInfosAsync(e.Charge, e.ProductNr, e.Weight, true).ContinueWith(SaveCurrentInfosAsyncCompleted);
        }

        private void SaveCurrentInfosAsyncCompleted(Task task)
        {
            if (task.Exception == null)
            {
                _log.InfoEx($"current infos saved", _sourceClass);
                
                GoToNextItem();
            }
            else
            {
                _log.ErrorEx($"save current infos failed", _sourceClass);
            }
        }

        private async Task UpdateWeighingItemInDBAsync()
        {
            _log.InfoEx($"update weighingitem {SelectedWeighingItemDetail.ToString()}", _sourceClass);

            await DatabaseExtension.UpdateWeighingInDBAsync(SelectedWeighingItemDetail);
        }

        private async Task SaveCurrentInfosAsync(string charge, string articleNr, double weight, bool isDone)
        {
            _log.InfoEx($"save current infos: {charge}, {articleNr}", _sourceClass);
            
            NotifyCanExecuteChanged();

            WeighingItem.Charge = charge;
            WeighingItem.ProductNr = articleNr;
            WeighingItem.Weight = RoundValue(weight);
            WeighingItem.IsDone = isDone;

            _log.InfoEx($"convert data to weighing model {WeighingItem.ToString()}", _sourceClass);

            if (isDone)
            {
                await DatabaseExtension.UpdateWeighingInDBAsync(WeighingItem);
                ChangeBackgroundForCompletedItem();
            }
            else
            {
                WeighingModel weighing = new WeighingModel
                {
                    Charge = WeighingItem.Charge,
                    ProductNr = WeighingItem.ProductNr,
                    OpenOrderId = WeighingItem.OpenOrderId,
                    RecipeItem = WeighingItem.RecipeItem,
                    RecipeItemId = WeighingItem.RecipeItemId,
                    TargetWeight = WeighingItem.TargetWeight,
                    Unit = WeighingItem.Unit,
                    Weight = WeighingItem.Weight,
                    IsDone = true,
                    ParentId = WeighingItem.Id
                };

                await DatabaseExtension.SaveWeighingInDBAsync(weighing);
            }

            UpdateCurrentRecipeView();
            ChangeBackgroundForSelectedItem();
        }

        private void GoToNextItem(bool skip = false)
        {
            if (WeighingItem == _weighingItems.Last())
            {
                if (_newState != ProcessState.Idle && _newState != ProcessState.Completed)
                {
                    _newState = ProcessState.LastItemReached;
                    _log.InfoEx($"new state: {_newState}", _sourceClass);
                }
                return;
            }

            _itemIndex++;
            _currentListIndex++;

            SetDeltaTracValues();

            if (WeighingItem.IsDone)
            {
                GoToNextItem();
                return;
            }
                
            NotifyCanExecuteChanged();
            UpdateCurrentRecipeView();
            ChangeBackgroundForSelectedItem();            
        }

        private void ChangeBackgroundForCompletedItem()
        {
            _log. InfoEx($"new background: LightGreen, index: {_itemIndex}", _sourceClass);
            RecipeView.UpdateBackground(_currentListIndex, LightGreen);
        }

        private void ChangeBackgroundForSelectedItem()
        {
            _log.InfoEx($"new background: LightBlue, index: {_itemIndex}", _sourceClass);
            RecipeView.UpdateBackground(_currentListIndex, LightBlue);
        }

        private void ChangeBackgroundForUncompletedItem()
        {
            _log.InfoEx($"new background: LightGray, index: {_itemIndex}", _sourceClass);
            RecipeView.UpdateBackground(_currentListIndex, LightGray);
        }
        
        private void UpdateCurrentRecipeView()
        {
            if (_currentListIndex == Globals.MaxWeighingItems)
            {
                CalculatedPageIndex++;
                _currentListIndex = 0;
            }

            RecipeView = new RecipeControlView(_homeNavigationFrame, _title, _weighingItems, WeighingRecipeWidth);
            RecipeView._viewModel.UpdateNextPage(_currentListIndex, CalculatedPageIndex);
            RecipeView._viewModel.WeighingItemSelected += _viewModel_WeighingItemSelected;
        }

        private bool CanExecuteGoNextCommand()
        {
            return _itemIndex < _weighingItems.Count && _itemIndex >= 0;
            //return _itemIndex < _weighingItems.Count && _itemIndex >= 0 && _hasContainer;
        }

        public ICommand GoNext => _goNextCommand = new DelegateCommand(GoNextCommandExecute, CanExecuteGoNextCommand);

        #endregion

        #endregion
        
        #region Event OnRecipeCompleted

        public event EventHandler<RecipeCompletedEventArgs> RecipeCompleted;

        protected virtual void OnRecipeCompleted(RecipeCompletedEventArgs e)
        {
            if (RecipeCompleted != null)
                RecipeCompleted(this, e);
        }

        public class RecipeCompletedEventArgs : EventArgs
        {

            public RecipeCompletedEventArgs()
            {
            }
        }

        #endregion

        #region Event OnCancelled

        public event EventHandler<CancelledEventArgs> Cancelled;

        protected virtual void OnCancelled(CancelledEventArgs e)
        {
            if (Cancelled != null)
                Cancelled(this, e);
        }

        public class CancelledEventArgs : EventArgs
        {

            public CancelledEventArgs()
            {
            }
        }

        #endregion
    }
}
