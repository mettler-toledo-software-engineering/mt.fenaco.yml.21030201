﻿using log4net;
using mt.fenaco.Infrastructure;
using mt.fenaco.Logic;
using mt.fenaco.Models;
using mt.fenaco.Views;
using mt.fenaco.Views.Controls;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mt.fenaco.ViewModels
{
    public class MainProcessViewModel : PropertyChangedBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(MainProcessViewModel);

        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;
                
        private DelegateCommand _goBackCommand;
        private DelegateCommand _goStartCommand;

        private RecipeModel _flowerRecipe = new RecipeModel(Globals.RecipeType.Flower);
        private RecipeModel _grassRecipe = new RecipeModel(Globals.RecipeType.Grass);

        private List<WeighingModel> _flowerRecipeItems = new List<WeighingModel>();
        private List<WeighingModel> _grassRecipeItems = new List<WeighingModel>();
        
        private InfoBoxView _infoBoxLoadRecipe;

        private bool _isLoading;

        public MainProcessViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, OrderModel order)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;

            Process.Order = order;

            InitializeProcess();
        }

        #region Initialize

        private void InitializeProcess()
        {
            ResetRecipeItems();
            UpdateView();
        }

        private void ResetRecipeItems()
        {
            _isLoading = false;
            _flowerRecipe.OriginalRecipeItems = new List<RecipeItemModel>();
            _grassRecipe.OriginalRecipeItems = new List<RecipeItemModel>();
        }

        #endregion

        #region Update Fields

        private void LoadRecipeControls()
        {
            OrderModel order = Process.Order;
            
            FlowerRecipeView = new RecipeControlView(_homeNavigationFrame, Localization.Get(Localization.Key.Flower), order.FlowerRecipe.OriginalRecipeItems, Globals.RecipeWidth);
            GrassRecipeView = new RecipeControlView(_homeNavigationFrame, Localization.Get(Localization.Key.Grass), order.GrassRecipe.OriginalRecipeItems, Globals.RecipeWidth);
        }
        
        private void NotifyCanExecuteChanged()
        {
            _goBackCommand?.NotifyCanExecuteChanged();
            _goStartCommand?.NotifyCanExecuteChanged();
        }

        public void UpdateView()
        {
            LoadRecipeControls();
            
            Status = Process.Order.IsNew ? Localization.Get(Localization.Key.New) : Localization.Get(Localization.Key.Stopped);
            OrderNr = Process.Order.Number.ToString();
            Name = Process.Order.Name;
            OriginPlace = Process.Order.OriginPlace;
            PartialWeight = $"{Globals.GetWeightAsString(Process.PartialWeight, Settings.Current.UnitInCSV)}";
        }

        #endregion

        #region Properties

        #region GrassRecipeView

        private RecipeControlView _grassRecipeView;

        public RecipeControlView GrassRecipeView
        {
            get
            {
                return _grassRecipeView;
            }
            set
            {
                if (value != _grassRecipeView)
                {
                    _grassRecipeView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region FlowerRecipeView

        private RecipeControlView _flowerRecipeView;

        public RecipeControlView FlowerRecipeView
        {
            get
            {
                return _flowerRecipeView;
            }
            set
            {
                if (value != _flowerRecipeView)
                {
                    _flowerRecipeView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion
        
        #region Status

        private string _status;

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                if (value != _status)
                {
                    _status = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region OrderNr

        private string _orderNr;

        public string OrderNr
        {
            get
            {
                return _orderNr;
            }
            set
            {
                if (value != _orderNr)
                {
                    _orderNr = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Name

        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region OriginPlace

        private string _originPlace;

        public string OriginPlace
        {
            get
            {
                return _originPlace;
            }
            set
            {
                if (value != _originPlace)
                {
                    _originPlace = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region PartialWeight

        private string _partialWeight;

        public string PartialWeight
        {
            get
            {
                return _partialWeight;
            }
            set
            {
                if (value != _partialWeight)
                {
                    _partialWeight = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

        #region Key Commands

        #region GoBack

        private void GoBackCommandExecute()
        {
            _homeNavigationFrame.Back();
        }

        private bool CanExecuteGoBackRecipesCommand()
        {
            return !_isLoading;
        }

        public ICommand GoBack => _goBackCommand = new DelegateCommand(GoBackCommandExecute, CanExecuteGoBackRecipesCommand);

        #endregion

        #region GoStart
        public ICommand GoStart => _goStartCommand = new DelegateCommand(GoStartCommandExecute, CanExecuteGoStartCommand);

        private bool CanExecuteGoStartCommand()
        {
            return !_isLoading;
        }

        private void GoStartCommandExecute()
        {
            _isLoading = true;
            _log.InfoEx("Start Fill process", _sourceClass);
            
            if (Process.Order.IsNew)
            {
                AmountInputView amountInputView = new AmountInputView(_homeNavigationFrame);
                amountInputView._viewModel.AmountSaved += _viewModel_AmountSaved;
                amountInputView.Show(_parent);
            }
            else
            {
                StartFill();
            }
        }

        private async void StartFill()
        {
            await DatabaseExtension.GetAllOrdersFromDBAsync();

            NavigateToFillProcessView();
        }

        private void NavigateToFillProcessView()
        {
            FillProcessView fillProcessView = new FillProcessView(_homeNavigationFrame);
            fillProcessView._viewModel.Cancelled += _viewModel_FillFlowerCancelled;
            fillProcessView._viewModel.RecipeCompleted += _viewModel_RecipeCompleted;

            _homeNavigationFrame.NavigateTo(fillProcessView, TransitionAnimation.LeftToRight);
        }

        private void _viewModel_RecipeCompleted(object sender, FillProcessViewModel.RecipeCompletedEventArgs e)
        {
            RecipeCompleted();

            _homeNavigationFrame.Back();
            _homeNavigationFrame.Back();
        }
        
        private void _viewModel_AmountSaved(object sender, AmountInputViewModel.AmountSavedEventArgs e)
        {
            _isLoading = true;

            NotifyCanExecuteChanged();            
            ShowLoadRecipeMessage();

            SaveOpenOrderAsync(e.Amount).ContinueWith(OnSaveOpenOrderToDatabaseCompleted);
        }

        private void InitializeWeighingItems()
        {
            RecipeModel flowerRecipe = Process.Order.FlowerRecipe;
            RecipeModel grassRecipe = Process.Order.GrassRecipe;

            int openOrderId = Process.OpenOrder.Id;
            int amount = Process.OpenOrder.Amount;

            for (int i = 0; i < amount; i++)
            {
                _flowerRecipeItems.AddRange(flowerRecipe.ConvertToWeighingModels(openOrderId));
                _grassRecipeItems.AddRange(grassRecipe.ConvertToWeighingModels(openOrderId));
            }
            
            _flowerRecipe.WeighingItems = _flowerRecipeItems.OrderBy(o => o.RecipeItem.NameBot).ToList();
            _grassRecipe.WeighingItems = _grassRecipeItems.OrderBy(o => o.RecipeItem.NameBot).ToList();
        }

        private async Task SaveWeighingsToDatabaseAsync()
        {
            List<WeighingModel> weighings = new List<WeighingModel>();
            weighings.AddRange(_flowerRecipe.WeighingItems);
            weighings.AddRange(_grassRecipe.WeighingItems);

            foreach (WeighingModel item in weighings)
            {
                await DatabaseExtension.SaveWeighingInDBAsync(item);
            }
        }

        private void OnSaveWeighingsToDatabaseCompleted(Task task)
        {
            CloseLoadRecipeMessage();

            if (task.Exception != null)
            {
                _log.ErrorEx("save weighings to database failed!", _sourceClass);
                return;
            }

            _log.InfoEx("weighing saved in database", _sourceClass);

            _isLoading = false;
            NotifyCanExecuteChanged();
            
            StartFill();
        }
        
        private async Task SaveOpenOrderAsync(int amount)
        {
            OpenOrderModel openOrder = new OpenOrderModel
            {
                Amount = amount,
                OrderId = Process.Order.Id
            };

            int id = await DatabaseExtension.SaveOpenOrderAndGetIdAsync(openOrder);

            Process.OpenOrder = openOrder;
            Process.OpenOrder.Id = id;
        }

        private void OnSaveOpenOrderToDatabaseCompleted(Task task)
        {
            if (task.Exception != null)
            {
                CloseLoadRecipeMessage();
                _log.ErrorEx("save open order to database failed!", _sourceClass);
                return;
            }

            _log.InfoEx("open Order in database", _sourceClass);

            InitializeWeighingItems();

            SaveWeighingsToDatabaseAsync().ContinueWith(OnSaveWeighingsToDatabaseCompleted);
        }

        private void _viewModel_FillFlowerCancelled(object sender, FillProcessViewModel.CancelledEventArgs e)
        {
            _log.ErrorEx("flower recipe filling cancelled", _sourceClass);
            _homeNavigationFrame.Back();
        }

        private void RecipeCompleted()
        {
            _log.InfoEx("completing recipe...", _sourceClass);

            CsvFileExtension.WriteCsvFile(_parent, Process.Order, Process.OpenOrder);
        }

        #region InfoBox LoadRecipeMessage

        private void ShowLoadRecipeMessage()
        {
            string title = Localization.Get(Localization.Key.LoadRecipeTitle);
            string message = Localization.Get(Localization.Key.LoadRecipeMessage);

            _infoBoxLoadRecipe = new InfoBoxView(title, message, MessageBoxIcon.Information, MessageBoxButtons.OK, false);
            _infoBoxLoadRecipe.Show(_parent);
        }

        private void CloseLoadRecipeMessage()
        {
            if (_infoBoxLoadRecipe != null)
            {
                _infoBoxLoadRecipe.Close();
            }
        }

        #endregion
        
        #endregion

        #endregion
    }
}
