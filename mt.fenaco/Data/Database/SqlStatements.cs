﻿namespace mt.fenaco.Data.Database
{
    public static class SqlStatements
    {
        public static string GetSqlStatement<T>(string parameterName, SqlStatementTypes statementType)
        {
            string sql = "";
            string tablename = $"{typeof(T).Name}s";

            if (tablename.StartsWith("I") && char.IsUpper(tablename, 1))
            {
                tablename = tablename.Substring(1);
            }

            switch (statementType)
            {
                case SqlStatementTypes.SelectFromWhere:
                    sql = $"select * from {tablename} where {parameterName} = @{parameterName} ";
                    break;
                case SqlStatementTypes.DeleteFromWhere:
                    sql = $"delete from {tablename} where {parameterName} < @{parameterName} ";
                    break;
            }
            return sql;
        }

        public static string GetSqlStatement<T>(string parameterName1, string parameterName2, SqlStatementTypes statementType)
        {
            string sql = "";
            string tablename = $"{typeof(T).Name}s";

            if (tablename.StartsWith("I") && char.IsUpper(tablename, 1))
            {
                tablename = tablename.Substring(1);
            }

            switch (statementType)
            {

                case SqlStatementTypes.SelectBetween:
                    sql = $"select * from {tablename} where Zeitstempel BETWEEN @{parameterName1} AND @{parameterName2}";
                    break;
            }
            return sql;
        }
    }
}
