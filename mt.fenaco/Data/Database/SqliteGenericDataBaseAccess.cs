﻿using log4net;
using mt.fenaco.Infrastructure;
using MT.Singularity.Logging;

using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using Dapper.Contrib.Extensions;
using Dapper;
using System;
using System.Threading.Tasks;

namespace mt.fenaco.Data.Database
{
    public class SqliteGenericDataBaseAccess<T> : IDataBaseConnection<T> where T : class
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(SqliteGenericDataBaseAccess<T>);

        #region DeleteDateAsync
        public Task<bool> DeleteDataAsync(T dataEntry)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        var result = await cnn.DeleteAsync(dataEntry);

                        _log.InfoEx($"deleted {dataEntry} of type {typeof(T).Name}", _sourceClass);

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"delete {dataEntry} of type {typeof(T).Name} failed", _sourceClass, ex);
                    return false;
                }

            });
            return task;
        }
        #endregion

        #region DeleteDataOlderThenAsync
        public Task<bool> DeleteDataOlderThenAsync(int months)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        var p = new DynamicParameters();
                        string parameterName = "Zeitstempel";

                        DateTime param = DateTime.Now.Date.AddMonths(-months);
                        p.Add($"@{parameterName}", param);

                        string sql = SqlStatements.GetSqlStatement<T>(parameterName, SqlStatementTypes.DeleteFromWhere);
                        await cnn.QueryAsync<T>(sql, p);
                        _log.InfoEx($"executed query{sql}");
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"delete of type {typeof(T).Name} failed", _sourceClass, ex);
                    return false;
                }
            });
            return task;
        }
        #endregion

        #region GetDataBetweenTwoDatesAsync
        public Task<List<T>> GetDataBetweenTwoDatesAsync(DateTime start, DateTime end)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        var p = new DynamicParameters();
                        string parameterName1 = "start";
                        string parameterName2 = "end";

                        DateTime param1 = start;
                        DateTime param2 = end;

                        p.Add($"@{parameterName1}", param1);
                        p.Add($"@{parameterName2}", param2);

                        string sql = SqlStatements.GetSqlStatement<T>(parameterName1, parameterName2, SqlStatementTypes.SelectBetween);
                        var result = await cnn.QueryAsync<T>(sql, p);
                        _log.InfoEx($"executed query{sql}");
                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"delete of type {typeof(T).Name} failed", _sourceClass, ex);
                    return null;
                }

            });
            return task;
        }
        #endregion

        #region GetAllDataEntriesAsync
        public Task<List<T>> GetAllDataEntriesAsync()
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        var result = await cnn.GetAllAsync<T>();

                        _log.InfoEx($"got all data entries of type { typeof(T).Name}", _sourceClass);

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"getting all entries of type {typeof(T).Name} from database failed", _sourceClass, ex);
                    return null;
                }

            });
            return task;
        }
        #endregion

        #region GetEntryByIdAsync
        public Task<T> GetEntryByIdAsync(int id)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        var result = await cnn.GetAsync<T>(id);

                        _log.InfoEx($"got data with with {id} result : {result}", _sourceClass);

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"getting {typeof(T).Name} with id {id} from database failed", _sourceClass, ex);
                    return null;
                }

            });
            return task;
        }
        #endregion

        #region SaveDataAsync
        public Task<int> SaveDataAsync(T dataEntry)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        var result = await cnn.InsertAsync(dataEntry);

                        _log.Success($"{dataEntry} saved to database", _sourceClass);

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"saving {dataEntry} to database failed", _sourceClass, ex);
                    return 0;
                }

            });
            return task;
        }
        #endregion

        #region SaveAllDataAsync
        public Task<List<int>> SaveAllDataAsync(List<T> dataEntries)
        {
            var task = Task.Run(async () =>
            {
                List<int> resultList = new List<int>();
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        foreach (T dataEntry in dataEntries)
                        {
                            var result = await cnn.InsertAsync(dataEntry);
                            resultList.Add(result);

                            _log.Success($"{dataEntry} saved to database", _sourceClass);
                        }
                        return resultList;
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"saving multiple {typeof(T).Name} to database failed", _sourceClass, ex);
                    return resultList;
                }

            });
            return task;
        }
        #endregion

        #region UpdateDataAsnyc
        public Task<bool> UpdateDataAsnyc(T dataEntry)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        var result = await cnn.UpdateAsync(dataEntry);
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"update {dataEntry} to database failed", _sourceClass, ex);
                    return false;
                }

            });
            return task;
        }
        #endregion

        #region GetEntriesByParameterAsync
        public Task<List<T>> GetEntriesByParameterAsync(string parameterName, object parameterValue)
        {


            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SQLiteConnection(Globals.ConnectionString()))
                    {
                        var p = new DynamicParameters();

                        if (parameterValue is int)
                        {
                            int param = (int)parameterValue;
                            p.Add($"@{parameterName}", param);
                        }
                        else if (parameterValue is string)
                        {
                            string param = (string)parameterValue;
                            p.Add($"@{parameterName}", param);
                        }
                        else if (parameterValue is double)
                        {
                            double param = (double)parameterValue;
                            p.Add($"@{parameterName}", param);
                        }
                        else if (parameterValue is DateTime)
                        {
                            DateTime param = (DateTime)parameterValue;
                            p.Add($"@{parameterName}", param);
                        }
                        else
                        {
                            _log.ErrorEx($"unknwon paramter type used {(parameterValue)} with paramter {parameterName}", _sourceClass);
                        }

                        string sql = SqlStatements.GetSqlStatement<T>(parameterName, SqlStatementTypes.SelectFromWhere);

                        var result = await cnn.QueryAsync<T>(sql, p);

                        _log.InfoEx($"got data with with {parameterName} and value {parameterValue} result : {result}", _sourceClass);

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"getting {typeof(T).Name} with paramter {parameterName} and value {parameterValue} from database failed", _sourceClass, ex);
                    return null;
                }

            });
            return task;
        }
        #endregion 
    }
}
