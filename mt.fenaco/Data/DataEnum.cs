﻿namespace mt.fenaco.Data
{
    public enum SqlStatementTypes
    {
        SelectFromWhere,
        SelectBetween,
        DeleteFromWhere
    }

    public enum SqlResult
    {
        Failed
    }
}
