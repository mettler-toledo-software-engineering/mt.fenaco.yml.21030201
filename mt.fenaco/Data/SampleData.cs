﻿using mt.fenaco.Infrastructure;
using mt.fenaco.Models;
using System.Collections.Generic;

namespace mt.fenaco.Data
{
    public class SampleData
    {
        public RecipeModel GrassRecipe
        {
            get
            {
                List<RecipeItemModel> items = new List<RecipeItemModel>();

                //items.Add(new RecipeItemModel("", "Wiesen - Fuchsschwanz", "Alopecurus pratensis", 0.117, "kg"));
                //items.Add(new RecipeItemModel("", "Duftendes Ruchgras", "Anthoxanthum odoratum", 0.078, "kg"));
                //items.Add(new RecipeItemModel("", "Fromental", "Arrhenatherum elatius", 0.195, "kg"));
                //items.Add(new RecipeItemModel("", "Mittleres Zittergras", "Briza media", 0.117, "kg"));
                //items.Add(new RecipeItemModel("GE", "Aufrechte Trespe", "Bromus erectus", 0.974, "kg"));
                //items.Add(new RecipeItemModel("", "Schlaffe Segge", "Carex flacca", 0.039, "kg"));
                //items.Add(new RecipeItemModel("", "Rasen - Schmiele", "Deschampsia cespitosa", 0.019, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Schwingel", "Festuca pratensis", 0.273, "kg"));
                //items.Add(new RecipeItemModel("", "Rot - Schwingel ausläufertreibend", "Festuca rubra rubra", 1.169, "kg"));
                //items.Add(new RecipeItemModel("GE", "Flaum - Wiesenhafer", "Helictotrichon pubescens", 0.195, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Goldhafer", "Trisetum flavescens", 0.019, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Fuchsschwanz", "Alopecurus pratensis", 0.039, "kg"));

                return new RecipeModel(Globals.RecipeType.Grass, items);
            }
        }

        public RecipeModel FlowerRecipe
        {
            get
            {
                List<RecipeItemModel> items = new List<RecipeItemModel>();

                //items.Add(new RecipeItemModel("", "Wiesen - Schafgarbe", "Achillea millefolium", 0.002, "kg"));
                //items.Add(new RecipeItemModel("", "Kriechender Günsel", "Ajuga reptans", 0, "kg"));
                //items.Add(new RecipeItemModel("", "Wald - Bergminze", "Calamintha menthifolia", 0.004, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Schaumkraut", "Cardamine pratensis", 0.005, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Flockenblume", "Centaurea jacea", 0.035, "kg"));
                //items.Add(new RecipeItemModel("", "Kohldistel", "Cirsium oleraceum", 0.053, "kg"));
                //items.Add(new RecipeItemModel("", "Wirbeldost", "Clinopodium vulgare", 0.018, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Pippau", "Crepis biennis", 0.018, "kg"));
                //items.Add(new RecipeItemModel("GE", "Wilde Möhre", "Daucus carota", 0.035, "kg"));
                //items.Add(new RecipeItemModel("", "Wilde Karde", "Dipsacus fullonum", 0.018, "kg"));
                //items.Add(new RecipeItemModel("", "Wasserdost", "Eupatorium cannabinum", 0.005, "kg"));
                //items.Add(new RecipeItemModel("", "Zypressenblättrige Wolfsmilch", "Euphorbia cyparissias", 0.009, "kg"));
                //items.Add(new RecipeItemModel("", "Moor - Geissbart", "Filipendula ulmaria", 0.018, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Labkraut", "Galium mollugo", 0.009, "kg"));
                //items.Add(new RecipeItemModel("", "Echtes Labkraut", "Galium verum", 0.018, "kg"));
                //items.Add(new RecipeItemModel("", "Echtes Johanniskraut", "Hypericum perforatum", 0.002, "kg"));
                //items.Add(new RecipeItemModel("", "Feld - Witwenblume", "Knautia arvensis", 0.142, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Platterbse", "Lathyrus pratensis", 0.018, "kg"));
                //items.Add(new RecipeItemModel("", "Wiesen - Margerite", "Leucanthemum vulgare", 0.014, "kg"));
                //items.Add(new RecipeItemModel("GE", "Sumpf - Hornklee", "Lotus pedunculatus", 0.009, "kg"));
                //items.Add(new RecipeItemModel("", "Blut - Weiderich", "Lythrum salicaria", 0.002, "kg"));
                //items.Add(new RecipeItemModel("", "Sumpf - Vergissmeinnicht", "Myosotis scorpioides", 0.005, "kg"));
                //items.Add(new RecipeItemModel("", "Echter Dost", "Origanum vulgare", 0.002, "kg"));
                //items.Add(new RecipeItemModel("", "Pastinak", "Pastinaca sativa", 0.018, "kg"));
                //items.Add(new RecipeItemModel("", "Grosse Bibernelle", "Pimpinella major", 0.002, "kg"));
                //items.Add(new RecipeItemModel("", "Wald - Schlüsselblume", "Primula elatior", 0.001, "kg"));
                //items.Add(new RecipeItemModel("", "Kleine Brunelle", "Prunella vulgaris", 0.018, "kg"));
                //items.Add(new RecipeItemModel("GE", "Grosses Flohkraut", "Pulicaria dysenterica", 0.001, "kg"));
                //items.Add(new RecipeItemModel("", "Kleiner Wiesenknopf", "Sanguisorba minor", 0.088, "kg"));
                //items.Add(new RecipeItemModel("", "Rote Waldnelke", "Silene dioica", 0.088, "kg"));
                //items.Add(new RecipeItemModel("", "Kuckucks - Lichtnelke", "Silene flos - cuculi", 0.012, "kg"));
                //items.Add(new RecipeItemModel("", "Echte Betonie", "Stachys officinalis", 0.071, "kg"));
                //items.Add(new RecipeItemModel("", "Sumpf - Ziest", "Stachys palustris", 0.018, "kg"));
                //items.Add(new RecipeItemModel("", "Habermarch", "Tragopogon orientalis", 0.124, "kg"));
                //items.Add(new RecipeItemModel("", "Arznei - Baldrian", "Valeriana officinalis", 0.005, "kg"));
                //items.Add(new RecipeItemModel("", "Eisenkraut", "Verbena officinalis", 0.002, "kg"));
                //items.Add(new RecipeItemModel("", "Vogel - Wicke", "Vicia cracca", 0.035, "kg"));
                //items.Add(new RecipeItemModel("", "Zaun - Wicke", "Vicia sepium", 0.044, "kg"));

                return new RecipeModel(Globals.RecipeType.Flower, items);
            }
        }

        public SampleData()
        {

        }
    }
}
