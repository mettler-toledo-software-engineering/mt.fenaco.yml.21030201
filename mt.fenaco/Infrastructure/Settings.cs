﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace mt.fenaco.Infrastructure
{
    [Serializable]
    public class Settings
    {
#if IndPro
        public static string FileName = Path.Combine(SingularityEnvironment.RunningApplication.DataDirectory, "Settings.xml");
#else
        public static string FileName = Path.Combine(".\\Settings", "Settings.xml");
#endif
        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(Settings));
        private static Settings _current;

        #region Load
        private static Settings Load()
        {
            if (!File.Exists(FileName))
            {
                return new Settings();
            }
            else
            {
                try
                {
                    using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        return (Settings)Serializer.Deserialize(fs);
                    }
                }
                catch (Exception)
                {
                    return new Settings();
                }
            }
        }

        public static Settings Current
        {
            get
            {
                EnsureSettings();
                return _current;
            }
        }

        public static void EnsureSettings()
        {
            if (_current == null)
                _current = Load();
        }
        #endregion

        #region Article Properties
        public int ArticleNrRow { get; set; }
        public int ArticleNrCol { get; set; }
        public int NameRow { get; set; }
        public int NameCol { get; set; }
        public int OriginPlaceRow { get; set; }
        public int OriginPlaceCol { get; set; }
        #endregion

        #region Grass Properties
        public int GrassStartRow { get; set; }
        public int GrassHK { get; set; }
        public int GrassNameGerman { get; set; }
        public int GrassNameBot { get; set; }
        public int GrassTarget { get; set; }
        #endregion

        #region Flower Properties
        public int FlowerStartRow { get; set; }
        public int FlowerHK { get; set; }
        public int FlowerNameGerman { get; set; }
        public int FlowerNameBot { get; set; }
        public int FlowerTarget { get; set; }
        #endregion

        public string UnitInCSV { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            ArticleNrRow = 8;
            ArticleNrCol = 2;

            NameRow = 7;
            NameCol = 1;

            OriginPlaceRow = 3;
            OriginPlaceCol = 14;

            GrassStartRow = 14;
            GrassHK = 52;
            GrassNameGerman = 53;
            GrassNameBot = 54;
            GrassTarget = 58;

            FlowerStartRow = 14;
            FlowerHK = 67;
            FlowerNameGerman = 69;
            FlowerNameBot = 70;
            FlowerTarget = 74;

            UnitInCSV = "kg";
        }

        #region Save
        public void Save()
        {
            if (FileName == null) return;
            if (!Directory.Exists(FileName))
                Directory.CreateDirectory(Path.GetDirectoryName(FileName));

            using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Serializer.Serialize(fs, this);
            }
        }
        #endregion
    }
}
