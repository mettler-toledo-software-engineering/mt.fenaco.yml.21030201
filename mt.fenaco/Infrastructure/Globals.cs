﻿using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Drawing;
using MT.Singularity.Platform.Devices.Scale;
using System.Reflection;
using System;
using mt.fenaco.Models;
using log4net;
using MT.Singularity.Logging;
using System.Collections.Generic;
using mt.fenaco.Logic;
using System.IO;
using MT.Singularity.Platform;
using System.Configuration;
using System.Threading.Tasks;
using mt.fenaco.ViewModels;
using MT.Singularity;

namespace mt.fenaco.Infrastructure
{
    public static class Globals
    {
        #region RecipeType Enum
        
        public enum RecipeType
        {
            Grass = 0,
            Flower = 1
        }

        #endregion

        private static IScale _scale;
        
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(Globals);

        private static readonly SingularityEnvironment Environment = new SingularityEnvironment("mt.fenaco");

        public static string SQLIteX86Driver = Path.Combine(Environment.BinaryDirectory, "X86");
        public static string BarcodeReaderName = "Cino FuzzyScan";

        public static int RecipeWidth = 420;

        public static int WeighingRecipeWidth = 510;

        private static int WeightFactor = 1000;

        public static readonly SolidColorBrush LightGray = new SolidColorBrush(Colors.LightGray);
        public static readonly SolidColorBrush LightGreen = new SolidColorBrush(Colors.LightGreen);
        public static readonly SolidColorBrush LightBlue = new SolidColorBrush(Colors.LightBlue);
        public static readonly SolidColorBrush Red = new SolidColorBrush(Colors.Red);
        
        public const int MaxRecipeItems = 10;
        public const int MaxWeighingItems = 5;
        public const double UpperToleranceFactor = 0.1;
        public const double LowerToleranceFactor = -0.1;
        public const string IND930USBDrive = @"D:\";
        public const string ExcelPattern = "*.xls*";
        public static string TimeFormat = "HH:mm:ss";
        public static string DecimalFormat = "0.00";
        public static string ProjectNumber = "P21030201";
        public static Config.Configuration Config;

        public static string ConnectionString()
        {
            //um den Configuration Manager nutzen zu können muss die Referenz System.Configuration hinzugefügt werden
            //add reference --> assemblies-- > system.configuration
            return ConfigurationManager.ConnectionStrings["FenacoSqlite"].ConnectionString;
        }

        public static string GetVersion()
        {
            return "V " + Assembly.GetExecutingAssembly().GetName().Version.Major +
                   "." + Assembly.GetExecutingAssembly().GetName().Version.Minor +
                   "." + Assembly.GetExecutingAssembly().GetName().Version.Build;
        }

        public static double RoundValue(double value)
        {
            int digits = 3;
            return value > 0 ? Math.Round(value, digits) : value;
        }

        public static string GetWeightAsString(double weight, string unit)
        {
            return $"{String.Format("{0:0.000}", weight)} {unit}";
        }

        #region Scale Methods

        internal static void SetCurrentScale()
        {
            _log.InfoEx($"set current scale {_scale.FriendlyName}", _sourceClass);
            _scale = HomeScreenViewModel.Instance.Scale;
        }

        internal static IScale GetCurrentScale()
        {
            _scale = HomeScreenViewModel.Instance.Scale;
            return _scale;
        }

        public static string GetCurrentScaleUnit()
        {
            GetCurrentScale();
            return _scale.CurrentDisplayUnit.ToAbbreviation();
        }

        public static async void DoTare()
        {
            GetCurrentScale();
            if (_scale != null)
            {
                _log.InfoEx("Tare", _sourceClass);
                await _scale.TareAsync();
            }
        }

        public static async void DoPresetTare(string weight, WellknownWeightUnit unit)
        {
            GetCurrentScale();
            if (_scale != null)
            {
                _log.InfoEx("Tare", _sourceClass);
                await _scale.PresetTareAsync(weight, unit);
            }
        }

        public static async void DoZero()
        {
            GetCurrentScale();
            if (_scale != null)
            {
                _log.InfoEx("Zero", _sourceClass);
                await _scale.ZeroAsync();
            }
        }

        public static async void DoClearTare()
        {
            GetCurrentScale();
            if (_scale != null)
            {
                _log.InfoEx("Clear tare", _sourceClass);
                await _scale.ClearTareAsync();
            }
        }

        #endregion
        
        #region Get Orders from USB

        public static async Task<bool> GetAllOrdersFromUSBAndNetworkAsync(OrderSelectionViewModel viewModel)
        {
            _log.InfoEx($"get all orders from excel file", _sourceClass);
            string path = Config.ImportPath;

            List<OrderModel> orders = new List<OrderModel>();
            List<ExcelFile> excelFiles = GetAllExcelFiles(path);
            //excelFiles.AddRange(GetAllExcelFiles(path));

            if (excelFiles == null)
                return false;

            viewModel.ProgressBarMaximumValue = excelFiles.Count;

            foreach (ExcelFile file in excelFiles)
            {
                //++viewModel.ProgressBarValue;
                UpdateProgressBar(viewModel, file.GetFilename());

                _log.InfoEx($"convert file to order model", _sourceClass);
                OrderModel order = file.ToOrderModel();

                if (!IsAlreadyInDB(order))
                {
                    int result = await DatabaseExtension.SaveOrderAndGetIdAsync(order);
                    order.Id = result;
                    orders.Add(order);

                    await DatabaseExtension.SaveRecipesInDBAsync(order.Id, order.FlowerRecipe);
                    await DatabaseExtension.SaveRecipesInDBAsync(order.Id, order.GrassRecipe);

                    await DatabaseExtension.GetAllOrdersFromDBAsync();

                    _log.Success($"order '{order.ToString()}' saved in database", _sourceClass);
                }
                else
                {
                    _log.Hint($"order '{order.ToString()}' is already in database", _sourceClass);
                }
            }

            return true;
        }

        private static void UpdateProgressBar(OrderSelectionViewModel viewModel, string filename)
        {
            string file = Localization.Get(Localization.Key.File);
            string of = Localization.Get(Localization.Key.Of);
            string message = Localization.Get(Localization.Key.LoadingExcelFilesMessage);

            ++viewModel.ProgressBarValue;
            int value = viewModel.ProgressBarValue;
            int maxValue = viewModel.ProgressBarMaximumValue;
            viewModel.ProgressBarMessage = $"{file} ('{filename}') {value} {of} {maxValue} {message}";
        }

        public static bool IsAlreadyInDB(OrderModel order)
        {
            OrderModel res = DatabaseExtension.Orders.Find(i => i.Name == order.Name);

            return res != null;
        }

        private static List<ExcelFile> GetAllExcelFiles(string path)
        {
            _log.InfoEx($"get all excel files", _sourceClass);

            try
            {
                string[] files = Directory.GetFiles(path, ExcelPattern);

                List<ExcelFile> excelFiles = new List<ExcelFile>();

                try
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        _log.InfoEx($"convert excel file ('{files[i]}') {i + 1} of {files.Length}", _sourceClass);
                        excelFiles.Add(new ExcelFile(files[i]));
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorEx($"exception: {ex.Message}", _sourceClass);
                }

                return excelFiles;

            }
            catch (Exception ex)
            {
                _log.ErrorEx($"path '{path}' not found", _sourceClass, ex);
                return null;
            }
        }
        
        public static double ConvertFromKgToG(double weight)
        {
            return weight * WeightFactor;
        }

        public static double ConvertFromGToKg(double weight)
        {
            return weight / WeightFactor;
        }

        #endregion
    }
}
