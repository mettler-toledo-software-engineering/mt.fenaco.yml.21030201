﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Infrastructure;
using mt.fenaco.Config;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Logging;
using log4net;
using System.IO;
using mt.fenaco.Logic.USB;
using mt.fenaco.Logic.Connections;
using mt.fenaco.Logic.BarcodeReader;
using MT.Singularity.Platform.Devices;
using System.Linq;

namespace mt.fenaco.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(Bootstrapper);

        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }

        protected override async void InitializeApplication()
        {
            base.InitializeApplication();
            await InitializeCustomerService();
        }

        private async Task InitializeCustomerService()
        {
            try
            {
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var customerComponent = new Components(configurationStore, securityService, CompositionContainer);
                
                CompositionContainer.AddInstance<IComponents>(customerComponent);

                if (Directory.Exists(Globals.SQLIteX86Driver))
                {
                    if (!File.Exists(Path.Combine(Globals.SQLIteX86Driver, "SQLite.Interop.dll")))
                    {
                        byte[] clistAsByteArray = Properties.Resources.SQLite_Interop;
                        File.WriteAllBytes(Path.Combine(Globals.SQLIteX86Driver, "SQLite.Interop.dll"), clistAsByteArray);
                    }
                }

                var configuration = await customerComponent.GetConfigurationAsync();
                Globals.Config = configuration;
                
                SetupUsb();
                await SetupBarcodeReader(configuration, engine);
            }
            catch (Exception ex)
            {
                _log.ErrorEx($"Error Bootstrapper: {ex.Message}", _sourceClass);
            }
        }

        private async Task SetupBarcodeReader(Configuration configuration, IPlatformEngine engine)
        {
            IBarcodeReader barcodereader = null;
            try
            {
                var connection = new SerialDeviceConnection(configuration.BarcodeReaderComPort).GetDeviceConnection();
                barcodereader = new SerialBarcodeReader(connection, EndOfDataCharacter.CR);
                if (connection != null && barcodereader.Initialized)
                {
                    CompositionContainer.AddInstance(barcodereader);
                    Log4NetManager.ApplicationLogger.Info("barcode reader added to composition container");
                }
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Setup BarcodeReader Failed", ex);
            }

            if (barcodereader == null || barcodereader.Initialized == false)
            {
                try
                {
                    var interfaceservice = await engine.GetInterfaceServiceAsync();
                    var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
                    var allserialinterface = allinterfaces.OfType<ISerialInterface>();
                    var serialinterface = allserialinterface.FirstOrDefault();

                    if (serialinterface != null)
                    {
                        Log4NetManager.ApplicationLogger.Info($"interface: {serialinterface.FriendlyName} with port: {serialinterface.PortName} found");
                        var serialconnection = await serialinterface.CreateConnectionChannelAsync();
                        try
                        {
                            barcodereader = new SerialBarcodeReader(serialconnection);

                            CompositionContainer.AddInstance(barcodereader);
                            Log4NetManager.ApplicationLogger.Info("barcode reader added to composition container");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            Log4NetManager.ApplicationLogger.Error("Error SetupBarcodeReader", e);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

        }

        private void SetupUsb()
        {
            var usbHandler = new USBHandler(Globals.IND930USBDrive);
            CompositionContainer.AddInstance<IUSBHandler>(usbHandler);

            usbHandler.DeviceInsertedEvent += UsbHandlerOnDeviceInsertedEvent;
            usbHandler.DeviceRemovedEvent += UsbHandlerOnDeviceRemovedEvent;
        }

        private void UsbHandlerOnDeviceRemovedEvent(object sender, EventArgs e)
        {

        }

        private void UsbHandlerOnDeviceInsertedEvent(object sender, EventArgs e)
        {
        }

        private void SetupDevices()
        {
            try
            {
                var connection = new UsbDeviceConnection(Globals.BarcodeReaderName).GetDeviceConnection();
                if (connection != null)
                {
                    var barcodereader = new UsbBarcodeReader(connection, EndOfDataCharacter.CR);
                    CompositionContainer.AddInstance<IBarcodeReader>(barcodereader);
                }

            }
            catch (Exception ex)
            {
                _log.Error("Setup BarcodeReader Failed", ex);
            }


            var usbHandler = new USBHandler(Globals.IND930USBDrive);
            CompositionContainer.AddInstance<IUSBHandler>(usbHandler);
        }

        private void DisposeBarcodeScanner()
        {
            IBarcodeReader barcodereader;
            var success = CompositionContainer.TryResolve(out barcodereader);

            if (success)
            {
                var usbdevice = new USBDevice(Globals.BarcodeReaderName);
                if (usbdevice.GetPortName() == "")
                {
                    barcodereader.Dispose();
                }
            }
        }
    }
}
