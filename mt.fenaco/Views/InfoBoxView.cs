﻿using mt.fenaco.ViewModels;
using MT.Singularity.Presentation.Controls;

namespace mt.fenaco.Views
{
    /// <summary>
    /// Interaction logic for InfoBoxView
    /// </summary>
    public partial class InfoBoxView
    {
        public readonly InfoBoxViewModel ViewModel;
        
        public InfoBoxView(string title, string message, MessageBoxIcon icon, MessageBoxButtons buttons)
        {
            ViewModel = new InfoBoxViewModel(this, title, message, icon, buttons, true);

            InitializeComponents();
        }

        public InfoBoxView(string title, string message, MessageBoxIcon icon, MessageBoxButtons buttons, bool hasButton)
        {
            ViewModel = new InfoBoxViewModel(this, title, message, icon, buttons, hasButton);

            InitializeComponents();
        }
    }
}
