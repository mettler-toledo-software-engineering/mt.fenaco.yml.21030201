﻿using mt.fenaco.ViewModels;
using MT.Singularity.Presentation.Controls;

namespace mt.fenaco.Views
{
    /// <summary>
    /// Interaction logic for AmountInputView
    /// </summary>
    public partial class AmountInputView
    {
        public AmountInputViewModel _viewModel;

        public AmountInputView(AnimatedContentControl homeNavigationFrame)
        {
            _viewModel = new AmountInputViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }
    }
}
