﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace mt.fenaco.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class HomeScreen : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow _weightDisplayWindow;
        private MT.Singularity.Presentation.Controls.Navigation.AnimatedNavigationFrame homeNavigationFrame;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            _weightDisplayWindow = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            _weightDisplayWindow.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            _weightDisplayWindow.Height = 200;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _weightDisplayWindow.Height,() =>  _viewModel.WeightDisplayHeight,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            _weightDisplayWindow.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _weightDisplayWindow.Visibility,() =>  _viewModel.WeightDisplayShow,MT.Singularity.Expressions.BindingMode.OneWay,false);
            homeNavigationFrame = new MT.Singularity.Presentation.Controls.Navigation.AnimatedNavigationFrame();
            homeNavigationFrame.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(homeNavigationFrame);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(_weightDisplayWindow, internal2);
            this.Content = internal1;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[2];
    }
}
