﻿using MT.Singularity.Presentation.Controls;
namespace mt.fenaco.Views.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class RecipeItemControlView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Margin = new MT.Singularity.Presentation.Thickness(5);
            internal3.Width = 240;
            internal3.FontSize = ((System.Nullable<System.Int32>)18);
            internal3.Text = "Botanischer Name";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Text,() => _viewModel.RecipeItem.NameBot,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(5);
            internal6.Width = 40;
            internal6.FontSize = ((System.Nullable<System.Int32>)18);
            internal6.Text = "GE";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() => _viewModel.RecipeItem.OriginPlace,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleRight;
            internal7.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 5, 5);
            internal7.Width = 85;
            internal7.FontSize = ((System.Nullable<System.Int32>)18);
            internal7.Text = "SOLL";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Text,() => _viewModel.RecipeItem.FullTargetWeight,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal7);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2);
            internal1.Width = 400;
            internal1.Height = 30;
            this.Content = internal1;
            this.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4291348680u));
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => this.Background,() => _viewModel.Background,MT.Singularity.Expressions.BindingMode.OneWay,false);
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
