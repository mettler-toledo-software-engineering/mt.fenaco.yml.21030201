﻿using MT.Singularity.Presentation.Controls.Navigation;
using mt.fenaco.Models;
using mt.fenaco.ViewModels.Controls;
using MT.Singularity.Presentation.Drawing;
using System.Collections.Generic;
using mt.fenaco.Infrastructure;

namespace mt.fenaco.Views.Controls
{
    /// <summary>
    /// Interaction logic for RecipeControlView
    /// </summary>
    public partial class RecipeControlView
    {
        public readonly RecipeControlViewModel _viewModel;

        public RecipeControlView(AnimatedNavigationFrame homeNavigationFrame, string title, List<WeighingModel> weighingItems, int listWidth)
        {
            _viewModel = new RecipeControlViewModel(this, homeNavigationFrame, title, weighingItems, listWidth);
            InitializeComponents();
        }

        public RecipeControlView(AnimatedNavigationFrame homeNavigationFrame, string title, List<RecipeItemModel> recipeItems, int listWidth)
        {
            _viewModel = new RecipeControlViewModel(this, homeNavigationFrame, title, recipeItems, listWidth);
            InitializeComponents();
        }

        public void UpdateBackground(int itemIndex, SolidColorBrush color)
        {
            _viewModel.UpdateWeighingItemBackground(itemIndex, color);
        }

        public void ScrollOffset(int itemIndex)
        {
            int offset = 2 * itemIndex;
            OverviewList.ScrollToVertical(offset);
        }
    }
}
