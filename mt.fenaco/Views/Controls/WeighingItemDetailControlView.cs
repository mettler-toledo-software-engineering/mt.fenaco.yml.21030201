﻿using mt.fenaco.Models;
using mt.fenaco.ViewModels.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.fenaco.Views.Controls
{
    /// <summary>
    /// Interaction logic for WeighingItemDetailControlView
    /// </summary>
    public partial class WeighingItemDetailControlView
    {
        public readonly WeighingItemDetailControlViewModel _viewModel;

        public WeighingItemDetailControlView(AnimatedNavigationFrame homeNavigationFrame, WeighingModel weighing)
        {
            _viewModel = new WeighingItemDetailControlViewModel(this, homeNavigationFrame, weighing);
            InitializeComponents();
        }
    }
}
