﻿using MT.Singularity.Presentation.Controls;
namespace mt.fenaco.Views.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class RecipeControlView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Presentation.Controls.ListBox OverviewList;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            MT.Singularity.Presentation.Controls.StackPanel internal11;
            MT.Singularity.Presentation.Controls.Button internal12;
            MT.Singularity.Presentation.Controls.Image internal13;
            MT.Singularity.Presentation.Controls.Button internal14;
            MT.Singularity.Presentation.Controls.Image internal15;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.Margin = new MT.Singularity.Presentation.Thickness(5);
            internal4.FontSize = ((System.Nullable<System.Int32>)26);
            internal4.Text = "Recipe Title";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() => _viewModel.Title,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4);
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            OverviewList = new MT.Singularity.Presentation.Controls.ListBox();
            OverviewList.Margin = new MT.Singularity.Presentation.Thickness(5);
            OverviewList.Width = 420;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => OverviewList.Width,() =>  _viewModel.ListBoxWidth,MT.Singularity.Expressions.BindingMode.OneWay,false);
            OverviewList.Height = 420;
            OverviewList.BorderBrush = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278190080u));
            OverviewList.ScrollVertical = true;
            OverviewList.AllowSelection = false;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => OverviewList.ItemsSource,() =>  _viewModel.ItemList,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 0, 0);
            internal7.Text = "Seite";
            internal7.FontSize = ((System.Nullable<System.Int32>)14);
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 0, 0);
            internal8.Text = "1";
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Text,() => _viewModel.CurrentPage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8.FontSize = ((System.Nullable<System.Int32>)14);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 0, 0);
            internal9.Text = "von";
            internal9.FontSize = ((System.Nullable<System.Int32>)14);
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 10, 0);
            internal10.Text = "5";
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() => _viewModel.MaxPages,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10.FontSize = ((System.Nullable<System.Int32>)14);
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7, internal8, internal9, internal10);
            internal6.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal13 = new MT.Singularity.Presentation.Controls.Image();
            internal13.Source = "embedded://mt.fenaco/mt.fenaco.Images.ArrowLeft.al8";
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal12 = new MT.Singularity.Presentation.Controls.Button();
            internal12.Content = internal13;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal12.Width = 100;
            internal12.Height = 50;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Command,() => _viewModel.GoPageBackwards,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15 = new MT.Singularity.Presentation.Controls.Image();
            internal15.Source = "embedded://mt.fenaco/mt.fenaco.Images.ArrowRight.al8";
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal14 = new MT.Singularity.Presentation.Controls.Button();
            internal14.Content = internal15;
            internal14.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal14.Width = 100;
            internal14.Height = 50;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Command,() => _viewModel.GoPageForwards,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11 = new MT.Singularity.Presentation.Controls.StackPanel(internal12, internal14);
            internal11.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(OverviewList, internal6, internal11);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal5);
            internal2.Width = 420;
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal2.Width,() =>  _viewModel.ListBoxWidth,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2);
            internal1.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            this.Content = internal1;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[8];
    }
}
