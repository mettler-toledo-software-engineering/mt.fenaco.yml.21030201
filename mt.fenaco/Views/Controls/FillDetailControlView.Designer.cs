﻿using MT.Singularity.Presentation.Controls;
namespace mt.fenaco.Views.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class FillDetailControlView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.GroupPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.StackPanel internal10;
            MT.Singularity.Presentation.Controls.TextBlock internal11;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal4.FontSize = ((System.Nullable<System.Int32>)30);
            internal4.Text = "NameBot";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() => _viewModel.WeighingItem.RecipeItem.NameBot,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4);
            internal3.Width = 325;
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.FontSize = ((System.Nullable<System.Int32>)30);
            internal6.Text = "OriginPlace";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() => _viewModel.WeighingItem.RecipeItem.OriginPlace,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.Visibility = MT.Singularity.Presentation.Visibility.Visible;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Visibility,() => _viewModel.VisibilityOriginPlace,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6);
            internal5.Width = 325;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal2 = new MT.Singularity.Presentation.Controls.GroupPanel(internal3, internal5);
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2.Width = 650;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal9.FontSize = ((System.Nullable<System.Int32>)30);
            internal9.Text = "0.025 kg";
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() => _viewModel.NewTargetWeightToString,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9);
            internal8.Width = 325;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal11 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.FontSize = ((System.Nullable<System.Int32>)30);
            internal11.Text = "(max. 0.025 kg)";
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal11.Text,() => _viewModel.TargetWeightToString,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10 = new MT.Singularity.Presentation.Controls.StackPanel(internal11);
            internal10.Width = 325;
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8, internal10);
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal7.Width = 650;
            internal7.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 20);
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2, internal7);
            internal1.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4293322470u));
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal1.Background,() => _viewModel.Background,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal1.Width = 650;
            internal1.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal1.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 10);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[6];
    }
}
