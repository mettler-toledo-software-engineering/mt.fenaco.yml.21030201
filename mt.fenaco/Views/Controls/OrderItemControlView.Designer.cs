﻿using MT.Singularity.Presentation.Controls;
namespace mt.fenaco.Views.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class OrderItemControlView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.GroupPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.StackPanel internal10;
            MT.Singularity.Presentation.Controls.TextBlock internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.Button internal13;
            MT.Singularity.Presentation.Controls.Image internal14;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.FontSize = ((System.Nullable<System.Int32>)28);
            internal5.Text = "S9999.02";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Text,() => ViewModel.Order.Number,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(-10, 0, 0, 0);
            internal6.Source = "embedded://mt.fenaco/mt.fenaco.Images.Pause.al8";
            internal6.Visibility = MT.Singularity.Presentation.Visibility.Visible;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Visibility,() => ViewModel.VisibilityIsNew,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal6);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal4.Width = 200;
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.FontSize = ((System.Nullable<System.Int32>)28);
            internal8.Text = "UFA Wildblumenwiese TEST CH-G";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Text,() => ViewModel.Order.Name,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal9.FontSize = ((System.Nullable<System.Int32>)22);
            internal9.Text = "100 Greifensee";
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() => ViewModel.Order.OriginPlace,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal11.FontSize = ((System.Nullable<System.Int32>)18);
            internal11.Text = "Hinzugefügt:";
            internal11.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Added);
            internal11.AddTranslationAction(() => {
                internal11.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Added);
            });
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal12.FontSize = ((System.Nullable<System.Int32>)18);
            internal12.Text = "25.11.2020 15:14";
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Text,() => ViewModel.Order.Timestamp,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10 = new MT.Singularity.Presentation.Controls.StackPanel(internal11, internal12);
            internal10.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal10.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(internal8, internal9, internal10);
            internal7.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal7.Width = 570;
            internal14 = new MT.Singularity.Presentation.Controls.Image();
            internal14.Source = "embedded://mt.fenaco/mt.fenaco.Images.Delete.al8";
            internal13 = new MT.Singularity.Presentation.Controls.Button();
            internal13.Content = internal14;
            internal13.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal13.Height = 60;
            internal13.Width = 80;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.Command,() => ViewModel.GoDelete,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal7, internal13);
            internal3.Margin = new MT.Singularity.Presentation.Thickness(10, 0, 0, 0);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.GroupPanel(internal3);
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            internal1.Margin = new MT.Singularity.Presentation.Thickness(5);
            internal1.Width = 865;
            internal1.Height = 100;
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[6];
    }
}
