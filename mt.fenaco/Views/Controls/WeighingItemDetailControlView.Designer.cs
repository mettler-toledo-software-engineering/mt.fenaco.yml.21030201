﻿using MT.Singularity.Presentation.Controls;
namespace mt.fenaco.Views.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class WeighingItemDetailControlView : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Width = 60;
            internal3.FontSize = ((System.Nullable<System.Int32>)16);
            internal3.Text = "Charge";
            internal3.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Charge);
            internal3.AddTranslationAction(() => {
                internal3.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Charge);
            });
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.Width = 90;
            internal4.FontSize = ((System.Nullable<System.Int32>)16);
            internal4.Text = "1033757";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() => _viewModel.WeighingItem.Charge,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.Width = 90;
            internal6.FontSize = ((System.Nullable<System.Int32>)16);
            internal6.Text = "Produkt Nr.";
            internal6.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.ProductNumber);
            internal6.AddTranslationAction(() => {
                internal6.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.ProductNumber);
            });
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Width = 70;
            internal7.FontSize = ((System.Nullable<System.Int32>)16);
            internal7.Text = "S2956.01";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Text,() => _viewModel.WeighingItem.ProductNr,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal7);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal5.Margin = new MT.Singularity.Presentation.Thickness(5, 0, 0, 0);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.Width = 70;
            internal9.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleRight;
            internal9.FontSize = ((System.Nullable<System.Int32>)16);
            internal9.Text = "Gewicht";
            internal9.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Weight);
            internal9.AddTranslationAction(() => {
                internal9.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Weight);
            });
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.Width = 85;
            internal10.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleRight;
            internal10.FontSize = ((System.Nullable<System.Int32>)16);
            internal10.Text = "0.254 kg";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() => _viewModel.WeighingItem.WeightToString,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal10);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(5, 0, 0, 0);
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2, internal5, internal8);
            internal1.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1.Margin = new MT.Singularity.Presentation.Thickness(5, 0, 5, 0);
            internal1.Height = 30;
            this.Content = internal1;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4291348680u));
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => this.Background,() => _viewModel.Background,MT.Singularity.Expressions.BindingMode.OneWay,false);
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
