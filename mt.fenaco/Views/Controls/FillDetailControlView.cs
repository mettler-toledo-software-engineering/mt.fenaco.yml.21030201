﻿using mt.fenaco.Models;
using mt.fenaco.ViewModels.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.fenaco.Views.Controls
{
    /// <summary>
    /// Interaction logic for FillDetailControlView
    /// </summary>
    public partial class FillDetailControlView
    {
        public readonly FillDetailControlViewModel _viewModel;

        public FillDetailControlView(AnimatedNavigationFrame homeNavigationFrame, WeighingModel weighing, double newTarget)
        {
            _viewModel = new FillDetailControlViewModel(this, homeNavigationFrame, weighing, newTarget);
            InitializeComponents();
        }
    }
}
