﻿using log4net;
using mt.fenaco.Models;
using mt.fenaco.ViewModels.Controls;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Drawing;
using System;

namespace mt.fenaco.Views.Controls
{
    /// <summary>
    /// Interaction logic for WeighingItemControlView
    /// </summary>
    public partial class WeighingItemControlView
    {
        public readonly WeighingItemControlViewModel _viewModel;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(WeighingItemControlViewModel);

        public WeighingItemControlView(AnimatedNavigationFrame homeNavigationFrame, WeighingModel weighingItem)
        {
            _viewModel = new WeighingItemControlViewModel(this, homeNavigationFrame, weighingItem);
            InitializeComponents();
        }

        public void UpdateWeighingItemBackground(SolidColorBrush color)
        {
            _viewModel.UpdateWeighingItemBackground(color);
        }

        private void OnSelectedItemChanged(object sender, System.EventArgs e)
        {
            try
            {
                ListBox listBox = ((ListBox)sender);
                WeighingItemDetailControlView controlView = (WeighingItemDetailControlView)listBox.SelectedItem;
                controlView._viewModel.SelectItem();
            }
            catch (Exception ex)
            {
                _log.ErrorEx("item selection failed", _sourceClass, ex);
            }
        }
    }
}
