﻿using mt.fenaco.Models;
using mt.fenaco.ViewModels.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.fenaco.Views.Controls
{
    /// <summary>
    /// Interaction logic for OrderItemControlView
    /// </summary>
    public partial class OrderItemControlView
    {
        public readonly OrderItemControlViewModel ViewModel;

        public OrderItemControlView(AnimatedNavigationFrame homeNavigationFrame, OrderModel orderItem)
        {
            ViewModel = new OrderItemControlViewModel(this, homeNavigationFrame, orderItem);
            InitializeComponents();
        }

        public OrderModel GetOrderModel()
        {
            return ViewModel.Order;
        }
    }
}
