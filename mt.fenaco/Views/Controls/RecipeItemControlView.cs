﻿using mt.fenaco.ViewModels.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using mt.fenaco.Models;

namespace mt.fenaco.Views.Controls
{
    /// <summary>
    /// Interaction logic for RecipeItemControlView
    /// </summary>
    public partial class RecipeItemControlView
    {
        private readonly RecipeItemControlViewModel _viewModel;

        public RecipeItemControlView(AnimatedNavigationFrame homeNavigationFrame, RecipeItemModel recipeItem)
        {
            _viewModel = new RecipeItemControlViewModel(this, homeNavigationFrame, recipeItem);
            InitializeComponents();
        }
    }
}
