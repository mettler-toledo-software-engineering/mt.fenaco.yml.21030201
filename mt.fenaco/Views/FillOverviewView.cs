﻿using mt.fenaco.Models;
using mt.fenaco.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;
using System.Collections.Generic;

namespace mt.fenaco.Views
{
    /// <summary>
    /// Interaction logic for FillOverviewView
    /// </summary>
    public partial class FillOverviewView
    {
        public FillOverviewViewModel _viewModel;

        public FillOverviewView(AnimatedNavigationFrame homeNavigationFrame, List<WeighingModel> flowerWeighings, List<WeighingModel> grassWeighings)
        {
            _viewModel = new FillOverviewViewModel(this, homeNavigationFrame, flowerWeighings, grassWeighings);
            InitializeComponents();
        }
    }
}
