﻿using mt.fenaco.Models;
using mt.fenaco.ViewModels;
using MT.Singularity.Presentation.Controls;

namespace mt.fenaco.Views
{
    /// <summary>
    /// Interaction logic for AddManualInfosView
    /// </summary>
    public partial class AddManualInfosView
    {
        public AddManualInfosViewModel _viewModel;

        public AddManualInfosView(AnimatedContentControl homeNavigationFrame, WeighingModel weighingItem, double weight, bool isNew = true)
        {
            _viewModel = new AddManualInfosViewModel(this, homeNavigationFrame, weighingItem, weight, isNew);
            InitializeComponents();
        }
    }
}
