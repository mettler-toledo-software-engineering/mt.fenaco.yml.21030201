﻿using mt.fenaco.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.fenaco.Views
{
    /// <summary>
    /// Interaction logic for FillProcessView
    /// </summary>
    public partial class FillProcessView
    {
        public FillProcessViewModel _viewModel;
        
        public FillProcessView(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new FillProcessViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        protected override void OnFirstNavigation()
        {
            PanelDeltaTrac.Activate();
            _weightDisplayWindow.Activate();
            _viewModel.Initialize();

            base.OnFirstNavigation();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _viewModel.UnregisterEventsForViewModel();
            _viewModel.UnregisterScaleEvent();

            return base.OnNavigatingBack(nextPage);
        }
    }
}