﻿using MT.Singularity.Presentation.Controls;
namespace mt.fenaco.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class AmountInputView : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBox internal9;
            MT.Singularity.Presentation.Controls.StackPanel internal10;
            MT.Singularity.Presentation.Controls.Button internal11;
            MT.Singularity.Presentation.Controls.GroupPanel internal12;
            MT.Singularity.Presentation.Controls.Image internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            MT.Singularity.Presentation.Controls.Button internal15;
            MT.Singularity.Presentation.Controls.GroupPanel internal16;
            MT.Singularity.Presentation.Controls.Image internal17;
            MT.Singularity.Presentation.Controls.TextBlock internal18;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 0, 0);
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal3.FontSize = ((System.Nullable<System.Int32>)30);
            internal3.Text = "Rezeptdurchführungen";
            internal3.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.RecipeExecutions);
            internal3.AddTranslationAction(() => {
                internal3.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.RecipeExecutions);
            });
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Width = 200;
            internal7.FontSize = ((System.Nullable<System.Int32>)24);
            internal7.Text = "Anzahl";
            internal7.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Amount);
            internal7.AddTranslationAction(() => {
                internal7.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Amount);
            });
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal9 = new MT.Singularity.Presentation.Controls.TextBox();
            internal9.Width = 160;
            internal9.FontSize = ((System.Nullable<System.Int32>)28);
            internal9.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal9.Text = "1";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() => _viewModel.Amount,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9);
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal8);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5);
            internal4.Margin = new MT.Singularity.Presentation.Thickness(20, 60, 20, 0);
            internal11 = new MT.Singularity.Presentation.Controls.Button();
            internal11.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal11.Width = 180;
            internal11.Height = 80;
            internal11.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal11.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal13 = new MT.Singularity.Presentation.Controls.Image();
            internal13.Source = "embedded://mt.fenaco/mt.fenaco.Images.Cancel.al8";
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal14.Text = "Abbrechen";
            internal14.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Cancel);
            internal14.AddTranslationAction(() => {
                internal14.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Cancel);
            });
            internal14.FontSize = ((System.Nullable<System.Int32>)20);
            internal12 = new MT.Singularity.Presentation.Controls.GroupPanel(internal13, internal14);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal11.Content = internal12;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal11.Command,() => _viewModel.GoCancel,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15 = new MT.Singularity.Presentation.Controls.Button();
            internal15.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal15.Width = 180;
            internal15.Height = 80;
            internal15.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal15.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal17 = new MT.Singularity.Presentation.Controls.Image();
            internal17.Source = "embedded://mt.fenaco/mt.fenaco.Images.ok.al8";
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal17.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal18 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal18.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal18.Text = "OK";
            internal18.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Ok);
            internal18.AddTranslationAction(() => {
                internal18.Text = mt.fenaco.Localization.Get(mt.fenaco.Localization.Key.Ok);
            });
            internal18.FontSize = ((System.Nullable<System.Int32>)20);
            internal16 = new MT.Singularity.Presentation.Controls.GroupPanel(internal17, internal18);
            internal16.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal15.Content = internal16;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal15.Command,() => _viewModel.GoSave,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10 = new MT.Singularity.Presentation.Controls.StackPanel(internal11, internal15);
            internal10.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal10.Margin = new MT.Singularity.Presentation.Thickness(0, 60, 0, 20);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4, internal10);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
