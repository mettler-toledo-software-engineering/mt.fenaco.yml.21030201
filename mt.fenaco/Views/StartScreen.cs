﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using mt.fenaco.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.UserManagement;

namespace mt.fenaco.Views
{
    /// <summary>
    /// Interaction logic for StartScreen
    /// </summary>
    public partial class StartScreen
    {
        private readonly StartScreenViewModel _viewModel;

        public StartScreen(AnimatedNavigationFrame homeNavigationFrame, ISecurityService securityService)
        {
            _viewModel = new StartScreenViewModel(this, homeNavigationFrame, securityService);
            InitializeComponents();
        }

        #region Overrides

        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            base.OnFirstNavigation();
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            base.OnNavigationReturning(previousPage);
        }

        #endregion
    }
}
