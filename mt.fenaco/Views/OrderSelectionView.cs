﻿using mt.fenaco.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation;
using System;
using MT.Singularity.Presentation.Controls;
using mt.fenaco.Views.Controls;
using log4net;
using MT.Singularity.Logging;

namespace mt.fenaco.Views
{
    /// <summary>
    /// Interaction logic for OrderSelectionView
    /// </summary>
    public partial class OrderSelectionView
    {
        private readonly OrderSelectionViewModel _viewModel;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(OrderSelectionViewModel);

        public OrderSelectionView(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new OrderSelectionViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        #region Overrides

        protected override async void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            await _viewModel.CheckForFinishedRecipes();
            base.OnFirstNavigation();
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            _viewModel.LoadItems();
            base.OnNavigationReturning(previousPage);
        }

        #endregion

        private void OnSelectedItemChanged(object sender, System.EventArgs e)
        {
            try
            {
                ListBox listBox = ((ListBox)sender);
                OrderItemControlView controlView = (OrderItemControlView)listBox.SelectedItem;

                if (controlView != null)
                {
                    controlView.ViewModel.SelectItem();
                }
            }
            catch(Exception ex)
            {
                _log.ErrorEx("item selection failed", _sourceClass, ex);
            }
        }
    }
}
