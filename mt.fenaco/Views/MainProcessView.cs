﻿using MT.Singularity.Presentation;
using mt.fenaco.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;
using mt.fenaco.Models;

namespace mt.fenaco.Views
{
    /// <summary>
    /// Interaction logic for MainProcessView
    /// </summary>
    public partial class MainProcessView
    {
        private readonly MainProcessViewModel _viewModel;

        public MainProcessView(AnimatedNavigationFrame homeNavigationFrame, OrderModel order)
        {
            _viewModel = new MainProcessViewModel(this, homeNavigationFrame, order);
            InitializeComponents();
        }

        #region Overrides

        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            base.OnFirstNavigation();
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            _viewModel.UpdateView();
            base.OnNavigationReturning(previousPage);
        }

        #endregion
    }
}
