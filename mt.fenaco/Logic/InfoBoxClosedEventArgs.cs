﻿using MT.Singularity.Presentation.Controls;
using System;

namespace mt.fenaco.Logic
{
    public class InfoBoxClosedEventArgs : EventArgs
    {
        public DialogResult DialogResult { get; set; }

        public InfoBoxClosedEventArgs(DialogResult dialogResult)
        {
            DialogResult = dialogResult;
        }
    }
}
