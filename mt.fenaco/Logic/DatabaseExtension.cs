﻿using log4net;
using mt.fenaco.Data.Database;
using mt.fenaco.Models;
using MT.Singularity.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static mt.fenaco.Infrastructure.Globals;

namespace mt.fenaco.Logic
{
    public static class DatabaseExtension
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(DatabaseExtension);

        private static IDataBaseConnection<OrderModel> _orderModelBaseConnection = new SqliteGenericDataBaseAccess<OrderModel>();
        private static IDataBaseConnection<RecipeItemModel> _recipeModelBaseConnection = new SqliteGenericDataBaseAccess<RecipeItemModel>();
        private static IDataBaseConnection<OpenOrderModel> _openOrdersModelBaseConnection = new SqliteGenericDataBaseAccess<OpenOrderModel>();
        private static IDataBaseConnection<WeighingModel> _weighingModelBaseConnection = new SqliteGenericDataBaseAccess<WeighingModel>();

        public static List<OrderModel> Orders = new List<OrderModel>();
        public static List<OpenOrderModel> OpenOrders = new List<OpenOrderModel>();
        public static List<WeighingModel> Weighings = new List<WeighingModel>();

        public static List<WeighingModel> FlowerWeighings = new List<WeighingModel>();
        public static List<WeighingModel> GrassWeighings = new List<WeighingModel>();

        private static RecipeModel _flowerRecipe;
        private static RecipeModel _grassRecipe;
        private static List<RecipeItemModel> _recipeItems;

        private static async Task AddRecipeItemsToOrdersAsync()
        {
            await GetAllRecipeItemsFromDBAsync();

            foreach (OrderModel order in Orders)
            {
                GetRecipeItemsByOrderId(order.Id);

                order.FlowerRecipe = _flowerRecipe;
                order.GrassRecipe = _grassRecipe;
            }
        }

        private static void GetRecipeItemsByOrderId(int orderId)
        {
            List<RecipeItemModel>  flowerRecipeItems = _recipeItems
                .Where(item => item.OrderId == orderId && item.Type == (int)RecipeType.Flower)
                .OrderBy(o => o.NameBot)
                .ToList();

            List<RecipeItemModel> grassRecipeItems = _recipeItems
                .Where(item => item.OrderId == orderId && item.Type == (int)RecipeType.Grass)
                .OrderBy(o => o.NameBot)
                .ToList();

            _flowerRecipe = new RecipeModel(RecipeType.Flower, flowerRecipeItems);
            _grassRecipe = new RecipeModel(RecipeType.Grass, grassRecipeItems);
        }
        
        public static List<WeighingModel> GetWeighingDetailList(int parentId)
        {
            List<WeighingModel> weighings = Weighings.Where(item => item.ParentId == parentId).ToList();
            WeighingModel parent = Weighings.Find(item => item.Id == parentId);

            try
            {
                if (weighings != null)
                {
                    weighings.Add(parent);
                    weighings = weighings.OrderBy(item => item.Timestamp).ToList();
                }
            }
            catch(Exception ex)
            {
                _log.ErrorEx($"get weighing detail list failed", _sourceClass, ex);
            }

            return weighings;
        }

        public static List<WeighingModel> GetWeighingsByType(OrderModel order, RecipeType type)
        {
            List<WeighingModel> resultWeighings = new List<WeighingModel>();
            List<RecipeItemModel> recipeItems = type == RecipeType.Flower ? order.FlowerRecipe.OriginalRecipeItems : order.GrassRecipe.OriginalRecipeItems;

            try
            {
                OpenOrderModel openOrder = OpenOrders.Where(o => o.OrderId == order.Id).FirstOrDefault();
                List<WeighingModel> weighings = Weighings.Where(w => w.OpenOrderId == openOrder.Id).ToList();
                
                foreach (RecipeItemModel item in recipeItems)
                {
                    List<WeighingModel> tempItems = weighings.Where(w => w.RecipeItemId == item.Id).ToList();
                    tempItems.ForEach(i => i.TargetWeight = ConvertFromKgToG(item.TargetWeight));

                    resultWeighings.AddRange(tempItems);
                }
            }
            catch(Exception ex)
            {
                _log.ErrorEx($"get weighings by type failed", _sourceClass, ex);
            }       

            return resultWeighings;
        }

        public static List<WeighingModel> GetWeighingsByRecipeItemId(int recipeItemId)
        {
            return Weighings.Where(item => item.RecipeItemId == recipeItemId).ToList();
        }

        #region Get data from database

        public static async Task GetAllOrdersFromDBAsync()
        {
            //_log.InfoEx($"get all orders from database", _sourceClass);

            Orders = await _orderModelBaseConnection.GetAllDataEntriesAsync();

            await AddRecipeItemsToOrdersAsync();
            await GetAllOpenOrdersFromDBAsync();
            await GetAllWeighingsFromDBAsync();            
        }

        private static async Task GetAllRecipeItemsFromDBAsync()
        {
            //_log.InfoEx($"get all recipe items from database", _sourceClass);
            _recipeItems = await _recipeModelBaseConnection.GetAllDataEntriesAsync();
        }

        private static async Task GetAllOpenOrdersFromDBAsync()
        {
            //_log.InfoEx($"get all open orders from database", _sourceClass);

            OpenOrders = await _openOrdersModelBaseConnection.GetAllDataEntriesAsync();

            //_log.InfoEx($"weighings: {OpenOrders.Count}", _sourceClass);
        }

        public static async Task GetAllWeighingsFromDBAsync()
        {
            //_log.InfoEx($"get all weighings from database", _sourceClass);

            Weighings = await _weighingModelBaseConnection.GetAllDataEntriesAsync();

            LinkRecipeItemsToWeighings();

            //_log.InfoEx($"weighings: {Weighings.Count}", _sourceClass);
        }

        private static void LinkRecipeItemsToWeighings()
        {
            foreach (WeighingModel item in Weighings)
            {
                try
                {
                    //_log.InfoEx($"find open order...", _sourceClass);
                    OpenOrderModel openOrder = OpenOrders.FirstOrDefault(o => o.Id == item.OpenOrderId);
                    //_log.InfoEx($"find order...", _sourceClass);
                    OrderModel order = Orders.FirstOrDefault(o => o.Id == openOrder?.OrderId);
                    //_log.InfoEx($"find flower recipe items...", _sourceClass);
                    RecipeItemModel flowerRecipeItem = order?.FlowerRecipe.OriginalRecipeItems.FirstOrDefault(i => i.Id == item.RecipeItemId);
                    //_log.InfoEx($"find grass recipe items...", _sourceClass);
                    RecipeItemModel grassRecipeItem = order?.GrassRecipe.OriginalRecipeItems.FirstOrDefault(i => i.Id == item.RecipeItemId);

                    item.RecipeItem = flowerRecipeItem ?? grassRecipeItem;
                }
                catch(Exception ex)
                {
                    _log.ErrorEx($"could not find some of the data!", _sourceClass, ex);
                }
            }
        }

        #endregion

        #region update data in database

        public static async Task UpdateWeighingInDBAsync(WeighingModel weighing)
        {
            //_log.InfoEx($"update weighing: {weighing.ToString()} in database...", _sourceClass);

            var result = await _weighingModelBaseConnection.UpdateDataAsnyc(weighing);

            //_log.InfoEx($"weighing model updated: {result}", _sourceClass);

            await GetAllWeighingsFromDBAsync();
        }

        #endregion

        #region Save data in database

        public static async Task<int> SaveWeighingInDBAsync(WeighingModel weighing)
        {
            //_log.InfoEx($"saving weighing: {weighing.ToString()} into database...", _sourceClass);

            var result = await _weighingModelBaseConnection.SaveDataAsync(weighing);

           // _log.InfoEx($"saved weighing model in database: {result}", _sourceClass);
            await GetAllWeighingsFromDBAsync();

            return result;
        }

        public static async Task<int> SaveOrderAndGetIdAsync(OrderModel order)
        {
            int result = await _orderModelBaseConnection.SaveDataAsync(order);
            await GetAllOpenOrdersFromDBAsync();

            return result;
        }

        public static async Task<int> SaveOpenOrderAndGetIdAsync(OpenOrderModel order)
        {
            int result = await _openOrdersModelBaseConnection.SaveDataAsync(order);
            await GetAllOpenOrdersFromDBAsync();

            return result;
        }

        public static async Task SaveRecipesInDBAsync(int orderId, RecipeModel recipe)
        {
            foreach (RecipeItemModel item in recipe.OriginalRecipeItems)
            {
                item.OrderId = orderId;
                item.Type = (int)recipe.Type;
                int result = await _recipeModelBaseConnection.SaveDataAsync(item);
                item.Id = result;
            }

            await GetAllRecipeItemsFromDBAsync();
        }

        #endregion

        #region Delete data from database

        public static async Task<bool> DeleteWeighingAsync(WeighingModel weighing)
        {
            //_log.InfoEx($"delete weighing {weighing}...", _sourceClass);

            var result = await _weighingModelBaseConnection.DeleteDataAsync(weighing);

            await GetAllWeighingsFromDBAsync();

            return result;
        }

        public static async Task<bool> DeleteOpenOrderAndWeighingsAsync(OpenOrderModel openOrder)
        {
            List<WeighingModel> weighings = Weighings.Where(item => item.OpenOrderId == openOrder.Id).ToList();

            foreach (WeighingModel weighing in weighings)
            {
               // _log.InfoEx($"delete weighing {weighing}...", _sourceClass);
                var result = await _weighingModelBaseConnection.DeleteDataAsync(weighing);
            }

           // _log.InfoEx($"delete open order {openOrder}...", _sourceClass);
            var res = await _openOrdersModelBaseConnection.DeleteDataAsync(openOrder);

            await GetAllOpenOrdersFromDBAsync();
            await GetAllWeighingsFromDBAsync();

            return res;
        }
        
        public static async Task<bool> DeleteOrderAndRecipeItemsAsync(OrderModel order)
        {
            if (!order.IsNew)
            {
                var openOrder = OpenOrders.Where(item => item.OrderId == order.Id).FirstOrDefault();
                if (openOrder != null)
                {
                    await DeleteOpenOrderAndWeighingsAsync(openOrder);
                }
            }

            List<RecipeItemModel> recipeItems = _recipeItems.Where(item => item.OrderId == order.Id).ToList();

            foreach (RecipeItemModel recipeItem in recipeItems)
            {
                //_log.InfoEx($"delete recipe item {recipeItem}...", _sourceClass);
                var result = await _recipeModelBaseConnection.DeleteDataAsync(recipeItem);
            }

            //_log.InfoEx($"delete order {order}...", _sourceClass);
            var res = await _orderModelBaseConnection.DeleteDataAsync(order);

            await GetAllOrdersFromDBAsync();

            return res;
        }

        #endregion
    }
}
