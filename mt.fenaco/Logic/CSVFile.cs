﻿using CsvHelper;
using mt.fenaco.Models;
using System;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using static mt.fenaco.Infrastructure.Globals;
using mt.fenaco.Infrastructure;
using System.Text;

namespace mt.fenaco.Logic
{
    public class CsvFile
    {
        private CultureInfo _customCulture = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();

        private string _destination = "";
        private string _date = "";
        private string _path = "";
        private string _fileName = "";
        
        private int _index;
        
        public CsvFile()
        {
            InitializeDefaultValues();
        }

        private void InitializeDefaultValues()
        {
            _index = 0;
            //_delimiter = ";"; //_currentSettings.Delimiter;
            _date = $"{DateTime.Now:yyMMdd_HHmmss}";
            _path = Globals.Config.ExportPath; //IND930USBDrive != ""?  IND930USBDrive : Globals.Config.ExportPath;
        }
        
        public void WriteOrderToFile(OrderModel order, OpenOrderModel openOrder)
        {
            _fileName = $"mt_{_date}-{order.Number.ToString()}_{order.Name}.csv";

            if (Directory.Exists(_path))
            {
                _destination = Path.Combine(_path, _fileName);
                WriteRecords(order);
            }
            else
            {
                OnWriteRecordsFailed(new WriteRecordsFailedEventArgs($"The path '{_path}' does not exist!"));
            }
        }

        private List<WeighingModel> GetGroupedWeighings(List<WeighingModel> weighings)
        {
            List<WeighingModel> groupedWeighings = weighings
                .GroupBy(l => new
                {
                    l.RecipeItemId,
                    l.Charge
                })
                .Select(cl => new WeighingModel
                {
                    Charge = cl.First().Charge,
                    ProductNr = cl.First().ProductNr,
                    OpenOrderId = cl.First().OpenOrderId,
                    RecipeItemId = cl.First().RecipeItemId,
                    Weight = RoundValue(cl.Sum(c => c.Weight))
                }).ToList();

            return groupedWeighings;
        }

        private List<CsvModel> ConvertAllRecipeItems(OrderModel order, RecipeModel recipe, List<WeighingModel> weighings)
        {
            List<CsvModel> items = new List<CsvModel>();

            foreach (WeighingModel weighing in GetGroupedWeighings(weighings))
            {
                RecipeItemModel recipeItem = recipe.OriginalRecipeItems.Where(r => r.Id == weighing.RecipeItemId).FirstOrDefault();

                CsvModel model = new CsvModel
                {
                    Id = ++_index,
                    OrderNr = order.Number,
                    OrderName = order.Name,
                    Charge = weighing.Charge,
                    ProductNr = weighing.ProductNr,
                    Recipe = recipe.Title,
                    NameBot = recipeItem.NameBot,
                    TargetWeight = recipeItem.TargetWeight,
                    ActualWeight = ConvertFromGToKg(weighing.Weight),
                    Unit = recipeItem.Unit,
                };

                items.Add(model);
            }
            
            return items;
        }

        private void WriteRecords(OrderModel order)
        {
            try
            {
                List<CsvModel> flowerItems = ConvertAllRecipeItems(order, order.FlowerRecipe, DatabaseExtension.GetWeighingsByType(order, RecipeType.Flower));
                List<CsvModel> grassItems = ConvertAllRecipeItems(order, order.GrassRecipe, DatabaseExtension.GetWeighingsByType(order, RecipeType.Grass));

                using (var writer = new StreamWriter(_destination, false, Encoding.UTF8))
                using (var csv = new CsvWriter(writer, _customCulture))
                {
                    List<CsvModel> items = new List<CsvModel>();

                    items.AddRange(grassItems);
                    items.AddRange(flowerItems);
                    items = items.OrderBy(i => i.Id).ToList();

                    csv.WriteHeader<CsvModel>();
                    csv.NextRecord();

                    foreach (CsvModel item in items)
                    {
                        csv.WriteRecord(item);
                        csv.NextRecord();
                    }

                    OnRecordsWritten(new RecordsWrittenEventArgs());
                }

            }
            catch(Exception ex)
            {
                OnWriteRecordsFailed(new WriteRecordsFailedEventArgs(ex.Message));
            }
        }
        
        #region Event OnWriteRecordsFailed

        public event EventHandler<WriteRecordsFailedEventArgs> WriteRecordsFailed;

        protected virtual void OnWriteRecordsFailed(WriteRecordsFailedEventArgs e)
        {
            if (WriteRecordsFailed != null)
                WriteRecordsFailed(this, e);
        }

        public class WriteRecordsFailedEventArgs : EventArgs
        {
            public string Message { get; set; }

            public WriteRecordsFailedEventArgs(string message)
            {
                Message = message;
            }
        }

        #endregion

        #region Event OnRecordsWritten

        public event EventHandler<RecordsWrittenEventArgs> RecordsWritten;

        protected virtual void OnRecordsWritten(RecordsWrittenEventArgs e)
        {
            if (RecordsWritten != null)
                RecordsWritten(this, e);
        }

        public class RecordsWrittenEventArgs : EventArgs
        {

            public RecordsWrittenEventArgs()
            {
            }
        }

        #endregion
    }
}
