﻿using log4net;
using mt.fenaco.Infrastructure;
using mt.fenaco.Models;
using MT.Singularity.Logging;
using System;
using System.Collections.Generic;
using System.IO;

using OfficeOpenXml;

namespace mt.fenaco.Logic
{
    public class ExcelFile
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(ExcelFile);

        private ExcelPackage _excelPackage;
        private Settings _settings;
        private string _file;
        
        public ExcelFile(string file)
        {
            _settings = Settings.Current;
            _file = file;

            if (!OpenFile(_file))
                return;
        }

        public string GetFilename()
        {
            return _file;
        }

        public OrderModel ToOrderModel()
        {
            DateTime now = DateTime.Now;

            string name = GetCellValue(
                _settings.NameRow,
                _settings.NameCol);

            string articleNumber = GetCellValue(
                _settings.ArticleNrRow,
                _settings.ArticleNrCol);

            string originPlace = GetCellValue(
                _settings.OriginPlaceRow,
                _settings.OriginPlaceCol);

            RecipeModel flowerRecipe = ReadRecipe(Globals.RecipeType.Flower,
                _settings.FlowerStartRow,
                _settings.FlowerHK,
                _settings.FlowerNameGerman,
                _settings.FlowerNameBot,
                _settings.FlowerTarget,
                _settings.UnitInCSV);
            
            RecipeModel grassRecipe = ReadRecipe(Globals.RecipeType.Grass,
                _settings.GrassStartRow,
                _settings.GrassHK,
                _settings.GrassNameGerman,
                _settings.GrassNameBot,
                _settings.GrassTarget,
                _settings.UnitInCSV);


            OrderModel order = new OrderModel()
            {
                Name = name,
                Number = articleNumber,
                OriginPlace = originPlace,
                GrassRecipe = grassRecipe,
                FlowerRecipe = flowerRecipe,
                Timestamp = now,
                IsNew = true
            };

            _log.InfoEx($"converted order model: name = {name}, article nr = {articleNumber}", _sourceClass);

            return order;
        }

        #region Open Excel file
        private bool OpenFile(string file)
        {
            if (!File.Exists(file))
                return false;
            
            try
            {
                _excelPackage = new ExcelPackage(new FileInfo(file));
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                _log.ErrorEx(ex.Message, _sourceClass, ex);
                return false;
            }

            return true;
        }

        private void Info_Closed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Read Recipe

        public RecipeModel ReadRecipe(Globals.RecipeType type, int iRow, int iHK, int iNameGerman, int iNameBot, int iTarget, string unit)
        {
            _log.InfoEx($"read recipe from excel file", _sourceClass);

            List<RecipeItemModel> recipeItems = new List<RecipeItemModel>();

            int i = iRow;

            RecipeItemModel item = null;

            while(true)
            {
                item = ConvertCellValueToRecipeItem(i++, iHK, iNameGerman, iNameBot, iTarget, unit);

                if (item == null)
                    break;

                recipeItems.Add(item);
            }

            return new RecipeModel(type, recipeItems);
        }

        #region Helper Methods

        public RecipeItemModel ConvertCellValueToRecipeItem(int iRow, int iHK, int iNameGerman, int iNameBot, int iTarget, string unit)
        {
            _log.InfoEx($"convert cell value to recipe item", _sourceClass);

            try
            {
                string originPlage = GetCellValue(iRow, iHK);
                string nameGerman = GetCellValue(iRow, iNameGerman);
                string nameBot = GetCellValue(iRow, iNameBot);
                string target = GetCellValue(iRow, iTarget);
                target = target != "" ? target : "0";

                if (nameGerman == "" && nameBot == "")
                    return null;

                double targetWeight = Convert.ToDouble(target);

                return new RecipeItemModel(originPlage, nameGerman, nameBot, Globals.RoundValue(targetWeight), Settings.Current.UnitInCSV);
            }
            catch (Exception ex)
            {
                _log.ErrorEx($"exception: {ex.Message}", _sourceClass);
                return null;
            }
        }

        private string GetLastPart(string content)
        {
            string[] contentParts = content.Split(' ');
            return contentParts[contentParts.Length - 1];
        }

        private string GetCellValue(int row, int col)
        {
            try
            {
                ExcelWorksheet ws = _excelPackage.Workbook.Worksheets[0];

                var cell = ws.Cells[row, col];
                var cellContent = cell.Value;

                _log.InfoEx($"cel({row}, {col}), content: {cellContent}", _sourceClass);

                if (ws.Cells[row, col] != null && ws.Cells[row, col].Value != null)
                {
                    string value = ws.Cells[row, col].Value.ToString();
                    return value.Trim();
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                _log.ErrorEx($"exception: {ex.Message}", _sourceClass);
                return "";
            }
        }

        #endregion

        #endregion
    }
}
