﻿using mt.fenaco.Infrastructure;

namespace mt.fenaco.Models
{
    public static class Process
    {
        /// <summary>
        /// Current order (exported from excel file)
        /// </summary>
        public static OrderModel Order { get; set; }

        /// <summary>
        /// Current order (exported from excel file)
        /// </summary>
        public static OpenOrderModel OpenOrder { get; set; }

        /// <summary>
        /// All RecipeItems
        /// </summary>
        //public static Dictionary<int, RecipeItemModel> RecipeItems = new Dictionary<int, RecipeItemModel>();
        
        /// <summary>
        /// Original target weight of the order
        /// </summary>
        public static double PartialWeight
        {
            get
            {
                double flowers = 0, grass = 0;

                Order.FlowerRecipe.OriginalRecipeItems.ForEach(i =>
                {
                    flowers += i.TargetWeight;
                });

                Order.GrassRecipe.OriginalRecipeItems.ForEach(i =>
                {
                    grass += i.TargetWeight;
                });

                return flowers + grass;
            }
        }

        /// <summary>
        /// Original target weight of the order
        /// </summary>
        public static string PartialWeightToString
        {
            get
            {
                return $"{PartialWeight} {Settings.Current.UnitInCSV}";
            }
        }
    }
}
