﻿using System.IO.Ports;

namespace mt.fenaco.Logic.Connections
{
    public interface IDeviceConnection
    {
        SerialPort GetDeviceConnection();
    }
}
