﻿using System;

namespace mt.fenaco.Logic.USB
{
    public interface IUSBHandler
    {
        bool UsbDriveAvailable { get; }

        event EventHandler DeviceInsertedEvent;
        event EventHandler DeviceRemovedEvent;
    }
}
