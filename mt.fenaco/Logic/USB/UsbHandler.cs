﻿using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using System;
using System.IO;
using System.Management;

namespace mt.fenaco.Logic.USB
{
    [Export(typeof(IUSBHandler))]
    [InjectionBehavior(IsSingleton = true)]
    public class USBHandler : IUSBHandler
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(USBHandler);

        public bool UsbDriveAvailable { get; private set; }

        public event EventHandler DeviceInsertedEvent;
        public event EventHandler DeviceRemovedEvent;

        private string _usbDrivePath;

        public USBHandler(string usbdrivePath)
        {
            _usbDrivePath = usbdrivePath;
            UsbDriveAvailable = CheckUSBDirectory();
            RegisterForUsbEvents();
        }

        private bool CheckUSBDirectory()
        {
            return (Directory.Exists(_usbDrivePath));
        }

        private void RegisterForUsbEvents()
        {
            WqlEventQuery insertQuery = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity'");

            ManagementEventWatcher insertWatcher = new ManagementEventWatcher(insertQuery);
            insertWatcher.EventArrived += DeviceInserted;
            insertWatcher.Start();
            
            WqlEventQuery removeQuery = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity'");
            ManagementEventWatcher removeWatcher = new ManagementEventWatcher(removeQuery);
            removeWatcher.EventArrived += DeviceRemoved;
            removeWatcher.Start();
        }

        private void DeviceRemoved(object sender, EventArrivedEventArgs e)
        {
            _log.InfoEx("Device removed", _sourceClass);
            UsbDriveAvailable = CheckUSBDirectory();
            DeviceRemovedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void DeviceInserted(object sender, EventArrivedEventArgs e)
        {
            _log.InfoEx("Device inserted", _sourceClass);
            UsbDriveAvailable = CheckUSBDirectory();
            DeviceInsertedEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}