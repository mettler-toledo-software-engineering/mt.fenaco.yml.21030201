﻿using log4net;
using mt.fenaco.Models;
using mt.fenaco.Views;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using System.Threading.Tasks;

namespace mt.fenaco.Logic
{
    public static class CsvFileExtension
    {
        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(CsvFileExtension);
        
        private static InfoBoxView _infoBoxDeleteRecipe;
        private static InfoBoxView _infoBoxWriteCsvFile;
                 
        private static Visual _parent;
        private static OrderModel _orderToDelete;
        private static OpenOrderModel _openOrderToDelete;
        
        #region WriteCsvFile

        public static void WriteCsvFile(Visual parent, OrderModel order, OpenOrderModel openOrder)
        {
            _log.InfoEx("start writing csv file...", _sourceClass);

            _parent = parent;
            _orderToDelete = order;
            _openOrderToDelete = openOrder;

            ShowInfoBoxWriteCSVFile();

            CsvFile csv = new CsvFile();
            csv.RecordsWritten += Csv_RecordsWritten;
            csv.WriteRecordsFailed += Csv_WriteRecordsFailed;
            csv.WriteOrderToFile(order, openOrder);
        }

        #region Csv_WriteRecordsFailed

        private static void Csv_WriteRecordsFailed(object sender, CsvFile.WriteRecordsFailedEventArgs e)
        {
            _log.ErrorEx("write csv file failed!", _sourceClass);

            string title = Localization.Get(Localization.Key.WriteCSVFileFailed);
            string message = e.Message;

            ChangeInfoBoxWriteCSVFile(title, message);
            ShowButtonInfoBoxWriteCSVFile();
        }

        #endregion

        #region Csv_RecordsWritten

        private static void Csv_RecordsWritten(object sender, CsvFile.RecordsWrittenEventArgs e)
        {
            _log.InfoEx("csv file records written", _sourceClass);

            string title = Localization.Get(Localization.Key.DataSuccessfullySaved);
            string message = Localization.Get(Localization.Key.DataSuccessfullySavedMessage);

            ChangeInfoBoxWriteCSVFile(title, message);

            ShowButtonInfoBoxWriteCSVFile();
            StartDeletingOrderAndWeighings();
        }

        #endregion

        #endregion

        #region StartDeletingOrderAndWeighings

        private static void StartDeletingOrderAndWeighings()
        {
            ShowInfoBoxDeleteRecipe();

            _log.InfoEx("start deleting open order and weighings from database...", _sourceClass);
            DatabaseExtension.DeleteOpenOrderAndWeighingsAsync(_openOrderToDelete).ContinueWith(OnDeleteOpenOrderAndWeighingsAsyncCompleted);
        }

        #region OnDeleteOpenOrderAndWeighingsAsyncCompleted

        private static void OnDeleteOpenOrderAndWeighingsAsyncCompleted(Task task)
        {
            if (task.Exception != null)
            {
                _log.ErrorEx("deleting open order and weighings failed!", _sourceClass, task.Exception);

                string title = Localization.Get(Localization.Key.DeleteWeighing);
                string message = Localization.Get(Localization.Key.DeleteRecipeFailed);

                ChangeInfoBoxDeleteRecipe(title, message);
                ShowButtonInfoBoxDeleteRecipe();

                title = Localization.Get(Localization.Key.WriteCSVFile);
                message = Localization.Get(Localization.Key.WriteCSVFileFailedMessage);

                ChangeInfoBoxWriteCSVFile(title, message);
                ShowButtonInfoBoxWriteCSVFile();

                return;
            }

            _log.InfoEx("start deleting order and recipe items from database...", _sourceClass);
            DatabaseExtension.DeleteOrderAndRecipeItemsAsync(_orderToDelete).ContinueWith(OnDeleteOrderAndRecipeItemsAsyncCompleted);
        }

        #endregion

        #region OnDeleteOrderAndRecipeItemsAsyncCompleted

        private static void OnDeleteOrderAndRecipeItemsAsyncCompleted(Task task)
        {
            string title, message;

            if (task.Exception != null)
            {
                _log.ErrorEx("deleting order and recipe items failed!", _sourceClass, task.Exception);

                title = Localization.Get(Localization.Key.DeleteRecipeTitle);
                message = Localization.Get(Localization.Key.DeleteRecipeFailed);

                ChangeInfoBoxDeleteRecipe(title, message);
                ShowButtonInfoBoxDeleteRecipe();

                return;
            }

            _log.InfoEx("data deleted from database", _sourceClass);

            title = Localization.Get(Localization.Key.DataSuccessfullySaved);
            message = Localization.Get(Localization.Key.DataSuccessfullySavedMessage);

            ChangeInfoBoxDeleteRecipe(title, message);
            CloseInfoBoxDeleteRecipe();
        }

        #endregion

        #region InfoBox WriteCSVFile

        private static void ShowInfoBoxWriteCSVFile()
        {
            string title = Localization.Get(Localization.Key.WriteCSVFileTitle);
            string message = Localization.Get(Localization.Key.WriteCSVFile);

            _infoBoxWriteCsvFile = new InfoBoxView(title, message, MessageBoxIcon.Information, MessageBoxButtons.OK, false);
            _infoBoxWriteCsvFile.Show(_parent);
        }

        private static void ChangeInfoBoxWriteCSVFile(string title, string message)
        {
            _infoBoxWriteCsvFile.ViewModel.Title = title;
            _infoBoxWriteCsvFile.ViewModel.Information = message;
        }

        private static void ShowButtonInfoBoxWriteCSVFile()
        {
            _infoBoxWriteCsvFile.ViewModel.ShowButtonInBox();
        }

        #endregion

        #region InfoBox DeleteRecipe

        private static void ShowInfoBoxDeleteRecipe()
        {
            string title = Localization.Get(Localization.Key.DeleteRecipeTitle);
            string message = Localization.Get(Localization.Key.DeleteRecipeMessage);

            _infoBoxDeleteRecipe = new InfoBoxView(title, message, MessageBoxIcon.Information, MessageBoxButtons.OK, false);
            _infoBoxDeleteRecipe.Show(_parent);
        }

        private static void ChangeInfoBoxDeleteRecipe(string title, string message)
        {
            _infoBoxDeleteRecipe.ViewModel.Title = title;
            _infoBoxDeleteRecipe.ViewModel.Information = message;
        }
        private static void ShowButtonInfoBoxDeleteRecipe()
        {
            _infoBoxDeleteRecipe.ViewModel.ShowButtonInBox();
        }

        private static void CloseInfoBoxDeleteRecipe()
        {
            _infoBoxDeleteRecipe.Close();
        }

        #endregion

        #endregion
    }
}
