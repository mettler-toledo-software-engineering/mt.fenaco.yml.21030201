﻿using MT.Singularity.Composition;
using System.IO.Ports;

namespace mt.fenaco.Logic.BarcodeReader
{
    [Export(typeof(IBarcodeReader))]
    [InjectionBehavior(IsSingleton = true)]
    public class UsbBarcodeReader : BarcodeReader
    {

        public UsbBarcodeReader(SerialPort connection, EndOfDataCharacter character) : base(connection, character)
        {

        }
    }
}
