﻿namespace mt.fenaco.Logic.BarcodeReader
{
    public static class EnumToCharTranslator
    {
        public static string TranslateCharakterToString(this EndOfDataCharacter character)
        {
            switch (character)
            {
                case EndOfDataCharacter.CR: return "\r";
                case EndOfDataCharacter.LF: return "\n";
                case EndOfDataCharacter.CRLF: return "\r\n";
                case EndOfDataCharacter.ZPLCRLF: return "^XZ\r\n";
                default: return "";
            }
        }
    }
}
