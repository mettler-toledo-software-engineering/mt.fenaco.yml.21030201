﻿using System;
using System.IO.Ports;

namespace mt.fenaco.Logic.BarcodeReader
{
    public interface IBarcodeReader : IDisposable
    {
        event EventHandler<DataReceivedEventArgs> DataReceivedEvent;
        bool Initialized { get; }
        SerialPort UsedSerialPort { get; }
    }
}
