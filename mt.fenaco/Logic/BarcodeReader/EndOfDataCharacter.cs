﻿namespace mt.fenaco.Logic.BarcodeReader
{
    public enum EndOfDataCharacter
    {
        CR,
        LF,
        CRLF,
        ZPLCRLF
    }
}