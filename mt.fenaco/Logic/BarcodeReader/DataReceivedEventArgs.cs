﻿using System;

namespace mt.fenaco.Logic.BarcodeReader
{
    public class DataReceivedEventArgs : EventArgs
    {
        public string Data { get; set; }

        public string Charge { get; set; }
        public string ArticleName { get; set; }
        public string ArticleNumber { get; set; }

        public DataReceivedEventArgs(string data)
        {
            Data = data;

            ConvertData();
        }

        private void ConvertData()
        {
            //1033754;S2951.01;Calendula officinalis
            string[] content = Data.Split(';');

            Charge = content[0];
            ArticleNumber = content[1];
            ArticleName = content[2];
        }
    }
}