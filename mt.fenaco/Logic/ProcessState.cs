﻿namespace mt.fenaco.Logic
{
    public enum ProcessState
    {
        Idle,
        Initialize,
        Filling,
        StartFillFlower,
        StartFillGrass,
        LastItemReached,
        WriteCSVFile,
        Completed
    }
}
