﻿using Dapper.Contrib.Extensions;
using mt.fenaco.Infrastructure;
using System;
namespace mt.fenaco.Models
{
    [Table("RecipeItems")]
    public class RecipeItemModel
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public DateTime Timestamp { get; set; }
        public int Type { get; set; }
        public string NameGerman { get; }
        public string NameBot { get; }
        public string OriginPlace { get; set; }
        public double TargetWeight { get; }
        public string Unit { get; set; }
        [Computed]
        public string FullTargetWeight
        {
            get
            {
                return $"{TargetWeight} {Unit}";
            }
        }

        public RecipeItemModel()
        {

        }

        public RecipeItemModel(string originPlace, string nameGerman, string nameBot, double target, string unit)
        {
            Timestamp = DateTime.Now;
            OriginPlace = originPlace;
            NameGerman = nameGerman;
            NameBot = nameBot;
            TargetWeight = target;
            Unit = unit;
        }
        
        public override string ToString()
        {
            return $"{NameGerman} ({NameBot}) {FullTargetWeight}";
        }
    }
}
