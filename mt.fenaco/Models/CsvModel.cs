﻿using System;

namespace mt.fenaco.Models
{
    public class CsvModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string OrderNr { get; set; }
        public string OrderName { get; set; }
        public string ProductNr { get; set; }
        public string Charge { get; set; }
        public string Recipe { get; set; }
        public string NameBot { get; set; }
        public double TargetWeight { get; set; }
        public double ActualWeight { get; set; }
        public string Unit { get; set; }
        
        public CsvModel ()
        {
            Date = DateTime.Now.ToString("dd.MM.yyyy");
            Time = DateTime.Now.ToString("HH:ss");
        }
    }
}
