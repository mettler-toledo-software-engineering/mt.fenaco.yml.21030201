﻿using Dapper.Contrib.Extensions;
using System;

namespace mt.fenaco.Models
{
    [Table("OpenOrders")]
    public class OpenOrderModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int OrderId { get; set; }
        public int Amount { get; set; }

        public OpenOrderModel()
        {
            Timestamp = DateTime.Now;
        }
    }
}
