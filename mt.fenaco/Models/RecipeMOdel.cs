﻿using System.Collections.Generic;
using static mt.fenaco.Infrastructure.Globals;

namespace mt.fenaco.Models
{
    public class RecipeModel
    {
        public string Title { get; set; }
        public RecipeType Type { get; set; }
        public List<RecipeItemModel> OriginalRecipeItems { get; set; }
        public List<WeighingModel> WeighingItems { get; set; }

        public RecipeModel(RecipeType type)
        {
            Type = type;

            SetTitle();
        }

        public RecipeModel(RecipeType type, List<RecipeItemModel> items)
        {
            Type = type;
            OriginalRecipeItems = items;

            SetTitle();
        }

        private void SetTitle()
        {
            Title = Type == RecipeType.Flower ? Localization.Get(Localization.Key.Flower) : Localization.Get(Localization.Key.Grass);
        }

        public List<WeighingModel> ConvertToWeighingModels(int openOrderId)
        {
            List<WeighingModel> weighingModels = new List<WeighingModel>();

            foreach (RecipeItemModel item in OriginalRecipeItems)
            {
                WeighingModel weighingModel = new WeighingModel
                {
                    OpenOrderId = openOrderId,
                    RecipeItemId = item.Id,
                    RecipeItem = item,
                    TargetWeight = ConvertFromKgToG(item.TargetWeight),
                    Unit = GetCurrentScaleUnit()
                };

                weighingModels.Add(weighingModel);
            }

            return weighingModels;
        }
    }
}
