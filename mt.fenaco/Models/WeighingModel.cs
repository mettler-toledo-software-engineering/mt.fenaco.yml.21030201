﻿using Dapper.Contrib.Extensions;
using mt.fenaco.Infrastructure;
using mt.fenaco.Logic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace mt.fenaco.Models
{
    [Table("Weighings")]
    public class WeighingModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int RecipeItemId { get; set; }
        [Computed]
        public RecipeItemModel RecipeItem { get; set; }
        public int OpenOrderId { get; set; }
        public double Weight { get; set; }
        [Computed]
        public double TargetWeight { get; set; }
        public string ProductNr { get; set; }
        public string Charge { get; set; }
        public bool IsDone { get; set; }
        [Computed]
        public string WeightToString
        {
            get
            {
                return $"{Weight} {Globals.GetCurrentScaleUnit()}";
            }
        }

        [Computed]
        public string TargetWeightToString
        {
            get
            {
                return $"{TargetWeight} {Globals.GetCurrentScaleUnit()}";
            }
        }
        public string Unit { get; set; }
        public int ParentId { get; set; }
        [Computed]
        public string TotalWeightToString
        {
            get
            {
                List<WeighingModel> weighings = DatabaseExtension.GetWeighingDetailList(Id);
                double totalWeight = weighings.Sum(i => i.Weight);

                return $"{totalWeight} {Globals.GetCurrentScaleUnit()}";
            }
        }

        public WeighingModel()
        {
            Timestamp = DateTime.Now;
        }

        public override string ToString()
        {
            return $"open order id: {OpenOrderId}; recipe item id: {RecipeItemId}; weight: {Weight}; product nr: {ProductNr}; charge: {Charge}";
        }
    }
}
