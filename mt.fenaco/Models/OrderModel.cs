﻿using Dapper.Contrib.Extensions;
using System;

namespace mt.fenaco.Models
{
    [Table("Orders")]
    public class OrderModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        [Computed]
        public RecipeModel GrassRecipe { get; set; }
        [Computed]
        public RecipeModel FlowerRecipe { get; set; }
        public string OriginPlace { get; set; }
        [Computed]
        public bool IsNew { get; set; }

        public OrderModel()
        {
            Timestamp = DateTime.Now;
        }

        public override string ToString()
        {
            return $"Auftrag: {Number} - {Name}";
        }
    }
}
