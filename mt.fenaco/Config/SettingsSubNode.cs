﻿using System;
using System.Threading.Tasks;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Presentation;
using mt.fenaco.Logic;
using log4net;
using MT.Singularity.Platform.Infrastructure;

namespace mt.fenaco.Config
{
    class SettingsSubNode : GroupSetupMenuItem
    {
        private readonly Configuration _configuration;
        private readonly IComponents _customerComponent;
        private readonly IPlatformEngine _platformEngine;

        private static readonly ILog _log = Log4NetManager.ApplicationLogger;
        private static readonly string _sourceClass = nameof(SettingsSubNode);

        /// <summary>
        /// Initializes a new instance of the <see cref="MySubNodeOne"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="customerComponent">The customer component.</param>
        /// <param name="configuration">The configuration.</param>
        public SettingsSubNode(SetupMenuContext context, IComponents customerComponent, Configuration configuration, IPlatformEngine platformEngine)
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.Setup), configuration, customerComponent)
        {
            _customerComponent = customerComponent;
            _configuration = configuration;
            _platformEngine = platformEngine;
        }

        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public override Task ShowChildrenAsync()
        {
            try
            {
                var barcodeReaderComPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BarcodeScannerPort);
                var barcodeReaderComPortTarget = new TextSetupMenuItem(_context, barcodeReaderComPortTitle, _configuration, "BarcodeReaderComPort");
                
                var importPathTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.ImportPath);
                var importPathTarget = new TextSetupMenuItem(_context, importPathTitle, _configuration, "ImportPath");
                
                var exportPathTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.ExportPath);
                var exportPathTarget = new TextSetupMenuItem(_context, exportPathTitle, _configuration, "ExportPath");

                var minWeightContainerTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.MinWeightContainer);
                var minWeightContainerTarget = new TextSetupMenuItem(_context, minWeightContainerTitle, _configuration, "MinWeightContainer");

                // Titles for the my Setup parameters
                var usbUpdateResultTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.UpdateStatus);
                var usbUpdateResultTarget = new TextBlockSetupMenuItem(_context, usbUpdateResultTitle, _configuration, "UsbUpdateResult");

                //buttons können nicht in gruppen gelegt werden sondern müssen direkt an das child
                var btnSystemUpdate = new ImageButtonSetupMenuItem(_context, "embedded://mt.fenaco/mt.fenaco.Images.FlashDisk.al8", new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Update, 20, DoSystemUpdate);
                var btnReboot = new ImageButtonSetupMenuItem(_context, "embedded://mt.fenaco/mt.fenaco.Images.restart.al8", new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Reboot, 20, DoReboot);

                var rangeGroup = new GroupedSetupMenuItems(_context, usbUpdateResultTarget, barcodeReaderComPortTarget, importPathTarget, exportPathTarget);
                var containerGroup = new GroupedSetupMenuItems(_context, minWeightContainerTarget);

                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup, containerGroup, btnSystemUpdate, btnReboot);

                //Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1, rangeGroup2, button, rangeGroup3);
            }
            catch (Exception ex)
            {
                _log.ErrorEx($"Unexpected Exception: {ex.Message}", _sourceClass);
            }

            return TaskEx.CompletedTask;
        }

        private void DoSystemUpdate()
        {
            bool success;
            success = SystemUpdate.ExecuteUpdate();

            if (success == true)
            {
                _configuration.UsbUpdateResult = "Update copied.";
            }
            else
            {
                _configuration.UsbUpdateResult = "Update failed!";
            }
        }
   

        private void DoReboot()
        {
            _log.InfoEx($"Reboot device", _sourceClass);
            _platformEngine.RebootAsync();
            _log.InfoEx($"Reboot device done", _sourceClass);
        }
    }
}
