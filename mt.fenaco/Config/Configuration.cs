﻿using System.ComponentModel;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace mt.fenaco.Config
{
    [Component]
    public class Configuration : ComponentConfiguration
    {
        public Configuration()
        {
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string UsbUpdateResult
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }
        
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("COM12")]
        public virtual string BarcodeReaderComPort
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(@"D:\")]
        public virtual string ImportPath
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(@"D:\")]
        public virtual string ExportPath
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(200.0)]
        public virtual double MinWeightContainer
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }
    }
}
